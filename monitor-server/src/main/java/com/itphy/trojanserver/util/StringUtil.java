/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itphy.trojanserver.util;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.itphy.trojanserver.Param;

/**
 * 
 * @author eric \\$\\w*
 */
public class StringUtil {

	public static String replaceParam(String content, String pattern,
			List<String> params) {

		Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
		// 用Pattern类的matcher()方法生成一个Matcher对象
		Matcher m = p.matcher(content);
		StringBuffer sb = new StringBuffer();
		// 使用find()方法查找第一个匹配的对象
		boolean result = m.find();
		// 使用循环找出模式匹配的内容替换之,再将内容加到sb里
		while (result) {
			m.appendReplacement(sb, "moon");
			result = m.find();
		}
		// 最后调用appendTail()方法将最后一次匹配后的剩余字符串加到sb里；
		m.appendTail(sb);
		return sb.toString();
	}

	public static Boolean findPattern(String content, String pattern) {
		Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
		// 用Pattern类的matcher()方法生成一个Matcher对象
		Matcher m = p.matcher(content);
		// 使用find()方法查找第一个匹配的对象
		return m.find();
	}

	public static String replaceParam(String content, String pattern,
			String replaceData) {
		Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
		// 用Pattern类的matcher()方法生成一个Matcher对象
		Matcher m = p.matcher(content);
		StringBuffer sb = new StringBuffer();
		// 使用find()方法查找第一个匹配的对象
		boolean result = m.find();
		// 使用循环找出模式匹配的内容替换之,再将内容加到sb里
		while (result) {
			m.appendReplacement(sb, replaceData);
			result = m.find();
		}
		// 最后调用appendTail()方法将最后一次匹配后的剩余字符串加到sb里；
		m.appendTail(sb);

		return sb.toString();
	}

	public static String regexData(String content) {
		String sendData = content;
		if (StringUtil.findPattern(content, Param.$CURRENTMILLIONTIME)) {
			sendData = StringUtil.replaceParam(sendData, Param.$CURRENTMILLIONTIME,
					String.valueOf(System.currentTimeMillis()));
		}
		if (StringUtil.findPattern(content, Param.$DATE)) {
			sendData = StringUtil.replaceParam(sendData, Param.$DATE,
					String.valueOf(new Date(System.currentTimeMillis())));
		}
		return sendData;
	}

}
