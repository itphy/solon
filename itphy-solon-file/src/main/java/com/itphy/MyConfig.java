package com.itphy;

import com.itphy.conf.FastDFSProperties;
import com.itphy.util.FastDFSClient;
import org.csource.common.IniFileReader;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.TrackerGroup;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Objects;

@Configuration
public class MyConfig {

//    @Bean
//    public HikariDataSource db1(@Inject("${datasource.db1}") HikariDataSource ds) {
//        return ds;
//    }

    @Bean
    public FastDFSClient fastDFSClient(@Inject("dfs") FastDFSProperties fastDFSProperties) {
        try {
            FastDFSClient.init(fastDFSProperties);
            return new FastDFSClient();
        } catch (Exception e) {
            throw new RuntimeException("fastdfs 初始化失败！");
        }
    }


}
