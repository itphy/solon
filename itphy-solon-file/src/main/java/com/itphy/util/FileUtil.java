package com.itphy.util;

import org.noear.solon.core.handle.Context;

import java.io.*;

public class FileUtil {

    public static byte[] InputStreamToByteArray(InputStream is) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[1024];
        int temp;
        while ((temp = is.read(bytes)) != -1) {
            outputStream.write(bytes, 0, temp);
        }
        return outputStream.toByteArray();
    }

    public static boolean isWin(){
        return System.getProperties().getProperty("os.name").startsWith("Windows");
    }

    /**
     * 是否开启跨域
     * @param ctx
     * @param flag true 开启跨域, false 否
     */
    public static void isCross(Context ctx,boolean flag){
        if(flag){
            ctx.headerAdd("Access-Control-Allow-Origin","*");
        } else {
            ctx.headerAdd("Access-Control-Allow-Origin","http://love.itphy.com:9999");
        }
    }
}
