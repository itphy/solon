package com.itphy.util;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Maps {

    private Map<String,Object> data;

    public static Maps newInstance(String key,Object val){
        Maps maps = new Maps();
        maps.setData(new HashMap<>());
        maps.put(key, val);
        return maps;
    }

    public void setData(Map<String,Object> data){
        this.data=data;
    }

    public Maps put(String key,Object val){
        data.put(key, val);
        return this;
    }

    public Map<String,Object> getData(){
        return data;
    }

    /**
     *
     * @param t
     * @param template
     * @return
     */
    public static Map<String,Object> getTemplateMessageData(Object t,String template){
        Map<String,Object> dataMap = JSON.parseObject(JSON.toJSONString(t), Map.class);
        Map<String, Object> data = new HashMap<>();
        Matcher matcher = Pattern.compile("(\\{\\{)(\\w+)").matcher(template);
        while (matcher.find()) {
            data.put(matcher.group(2), Maps.newInstance("value",dataMap.get(matcher.group(2))).put("color", "#173177").getData());
        }
        return data;
    }
}
