package com.itphy;

import org.noear.solon.Solon;

public class SolonApplication {
    public static void main(String[] args) {
        Solon.start(SolonApplication.class, args,app->{
            app.enableWebSocket(true);//启用websocket
        });
    }
}
