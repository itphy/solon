package com.itphy.pojo;


import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class ResFile implements Serializable {

    private Long id;
    private Long autId;
    private String saveUrl;
    private String extName;
    private String fileType;
    private String fileName;
    private String desc;
    private Long createTime;



}
