package com.itphy.conf;

import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

@Component("dfs")
public class FastDFSProperties {
    @Inject("${connect_timeout}")
    public Integer connect_timeout;

    @Inject("${network_timeout}")
    public Integer network_timeout;

    @Inject("${charset}")
    public String charset;

    @Inject("${tracker_server}")
    public String tracker_server;

    @Inject("${http.tracker_http_port}")
    public Integer g_tracker_http_port;

    @Inject("${http.anti_steal_token}")
    public Boolean g_anti_steal_token;

    @Inject("${http.secret_key}")
    public String g_secret_key;
}
