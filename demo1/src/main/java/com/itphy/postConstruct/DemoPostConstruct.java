package com.itphy.postConstruct;

import com.itphy.mapper.SysAreaMapper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

@Component
public class DemoPostConstruct implements InitializingBean, BeanPostProcessor, CommandLineRunner {

    @Resource
    SysAreaMapper sysAreaMapper;

    @PostConstruct
    public void postConstruct() {
        System.out.println("****************************** PostConstruct 我执行了。。。");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("****************************** InitializingBean 我执行了。。。");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("****************************** preDestroy 我执行了。。。");
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("****************************** run 我执行了。。。");
        for (String arg : args) {
            System.out.println(arg);
        }
    }

}
