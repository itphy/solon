package com.itphy.pojo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class SysArea implements Serializable {

    private static final long serialVersionUID = 1626173693266L;


    /**
    * 主键
    * 地区标识
    * isNullAble:0
    */
    private String areaId;

    /**
    * 地区代码
    * isNullAble:0
    */
    private String areaCode;

    /**
    * 地区名称
    * isNullAble:0
    */
    private String areaName;

    /**
    * 级别(1:省（自治区、直辖市、特别行政区）；2:市（地区、自治州、盟及国家直辖市所属市辖区和县的汇总码）;3:县（市辖区、县级市、旗）;4:乡、镇（街道办事处）)
    * isNullAble:1
    */
    private String levels;

    /**
    * 第一、二位表示省（自治区、直辖市、特别行政区）
    * isNullAble:1
    */
    private String levelOne;

    /**
    * 第三、四位表示市（地区、自治州、盟及国家直辖市所属市辖区和县的汇总码）。其中，01-20，21-50表示地区（自治州、盟）,51-70表示省直辖市
    * isNullAble:1
    */
    private String levelTwo;

    /**
    * 第五、六位表示县（市辖区、县级市、旗）。01-18表示市辖区或地区（自治州、盟）辖县级市；21-80表示县（旗）；81-99表示省直辖县级市
    * isNullAble:1
    */
    private String levelThree;

    /**
    * 第七至九位表示乡、镇（街道办事处）
    * isNullAble:1
    */
    private String levelFour;

    /**
    * 状态(1:启用,0:禁用)，默认:1
    * isNullAble:1,defaultVal:1
    */
    private String areaStatus;

    /**
    * 坐标纬度
    * isNullAble:1
    */
    private String latitude;

    /**
    * 坐标经度
    * isNullAble:1
    */
    private String longitude;

    /**
    * 0正常省份1省级直辖市2.特别行政区
    * isNullAble:1
    */
    private String provType;

    /**
    * 首字母
    * isNullAble:1
    */
    private String initial;


    public void setAreaId(String areaId){this.areaId = areaId;}

    public String getAreaId(){return this.areaId;}

    public void setAreaCode(String areaCode){this.areaCode = areaCode;}

    public String getAreaCode(){return this.areaCode;}

    public void setAreaName(String areaName){this.areaName = areaName;}

    public String getAreaName(){return this.areaName;}

    public void setLevels(String levels){this.levels = levels;}

    public String getLevels(){return this.levels;}

    public void setLevelOne(String levelOne){this.levelOne = levelOne;}

    public String getLevelOne(){return this.levelOne;}

    public void setLevelTwo(String levelTwo){this.levelTwo = levelTwo;}

    public String getLevelTwo(){return this.levelTwo;}

    public void setLevelThree(String levelThree){this.levelThree = levelThree;}

    public String getLevelThree(){return this.levelThree;}

    public void setLevelFour(String levelFour){this.levelFour = levelFour;}

    public String getLevelFour(){return this.levelFour;}

    public void setAreaStatus(String areaStatus){this.areaStatus = areaStatus;}

    public String getAreaStatus(){return this.areaStatus;}

    public void setLatitude(String latitude){this.latitude = latitude;}

    public String getLatitude(){return this.latitude;}

    public void setLongitude(String longitude){this.longitude = longitude;}

    public String getLongitude(){return this.longitude;}

    public void setProvType(String provType){this.provType = provType;}

    public String getProvType(){return this.provType;}

    public void setInitial(String initial){this.initial = initial;}

    public String getInitial(){return this.initial;}
    @Override
    public String toString() {
        return "SysArea{" +
                "areaId='" + areaId + '\'' +
                "areaCode='" + areaCode + '\'' +
                "areaName='" + areaName + '\'' +
                "levels='" + levels + '\'' +
                "levelOne='" + levelOne + '\'' +
                "levelTwo='" + levelTwo + '\'' +
                "levelThree='" + levelThree + '\'' +
                "levelFour='" + levelFour + '\'' +
                "areaStatus='" + areaStatus + '\'' +
                "latitude='" + latitude + '\'' +
                "longitude='" + longitude + '\'' +
                "provType='" + provType + '\'' +
                "initial='" + initial + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private SysArea set;

        private ConditionBuilder where;

        public UpdateBuilder set(SysArea set){
            this.set = set;
            return this;
        }

        public SysArea getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends SysArea{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> areaIdList;

        public List<String> getAreaIdList(){return this.areaIdList;}


        private List<String> fuzzyAreaId;

        public List<String> getFuzzyAreaId(){return this.fuzzyAreaId;}

        private List<String> rightFuzzyAreaId;

        public List<String> getRightFuzzyAreaId(){return this.rightFuzzyAreaId;}
        private List<String> areaCodeList;

        public List<String> getAreaCodeList(){return this.areaCodeList;}


        private List<String> fuzzyAreaCode;

        public List<String> getFuzzyAreaCode(){return this.fuzzyAreaCode;}

        private List<String> rightFuzzyAreaCode;

        public List<String> getRightFuzzyAreaCode(){return this.rightFuzzyAreaCode;}
        private List<String> areaNameList;

        public List<String> getAreaNameList(){return this.areaNameList;}


        private List<String> fuzzyAreaName;

        public List<String> getFuzzyAreaName(){return this.fuzzyAreaName;}

        private List<String> rightFuzzyAreaName;

        public List<String> getRightFuzzyAreaName(){return this.rightFuzzyAreaName;}
        private List<String> levelsList;

        public List<String> getLevelsList(){return this.levelsList;}


        private List<String> fuzzyLevels;

        public List<String> getFuzzyLevels(){return this.fuzzyLevels;}

        private List<String> rightFuzzyLevels;

        public List<String> getRightFuzzyLevels(){return this.rightFuzzyLevels;}
        private List<String> levelOneList;

        public List<String> getLevelOneList(){return this.levelOneList;}


        private List<String> fuzzyLevelOne;

        public List<String> getFuzzyLevelOne(){return this.fuzzyLevelOne;}

        private List<String> rightFuzzyLevelOne;

        public List<String> getRightFuzzyLevelOne(){return this.rightFuzzyLevelOne;}
        private List<String> levelTwoList;

        public List<String> getLevelTwoList(){return this.levelTwoList;}


        private List<String> fuzzyLevelTwo;

        public List<String> getFuzzyLevelTwo(){return this.fuzzyLevelTwo;}

        private List<String> rightFuzzyLevelTwo;

        public List<String> getRightFuzzyLevelTwo(){return this.rightFuzzyLevelTwo;}
        private List<String> levelThreeList;

        public List<String> getLevelThreeList(){return this.levelThreeList;}


        private List<String> fuzzyLevelThree;

        public List<String> getFuzzyLevelThree(){return this.fuzzyLevelThree;}

        private List<String> rightFuzzyLevelThree;

        public List<String> getRightFuzzyLevelThree(){return this.rightFuzzyLevelThree;}
        private List<String> levelFourList;

        public List<String> getLevelFourList(){return this.levelFourList;}


        private List<String> fuzzyLevelFour;

        public List<String> getFuzzyLevelFour(){return this.fuzzyLevelFour;}

        private List<String> rightFuzzyLevelFour;

        public List<String> getRightFuzzyLevelFour(){return this.rightFuzzyLevelFour;}
        private List<String> areaStatusList;

        public List<String> getAreaStatusList(){return this.areaStatusList;}


        private List<String> fuzzyAreaStatus;

        public List<String> getFuzzyAreaStatus(){return this.fuzzyAreaStatus;}

        private List<String> rightFuzzyAreaStatus;

        public List<String> getRightFuzzyAreaStatus(){return this.rightFuzzyAreaStatus;}
        private List<String> latitudeList;

        public List<String> getLatitudeList(){return this.latitudeList;}


        private List<String> fuzzyLatitude;

        public List<String> getFuzzyLatitude(){return this.fuzzyLatitude;}

        private List<String> rightFuzzyLatitude;

        public List<String> getRightFuzzyLatitude(){return this.rightFuzzyLatitude;}
        private List<String> longitudeList;

        public List<String> getLongitudeList(){return this.longitudeList;}


        private List<String> fuzzyLongitude;

        public List<String> getFuzzyLongitude(){return this.fuzzyLongitude;}

        private List<String> rightFuzzyLongitude;

        public List<String> getRightFuzzyLongitude(){return this.rightFuzzyLongitude;}
        private List<String> provTypeList;

        public List<String> getProvTypeList(){return this.provTypeList;}


        private List<String> fuzzyProvType;

        public List<String> getFuzzyProvType(){return this.fuzzyProvType;}

        private List<String> rightFuzzyProvType;

        public List<String> getRightFuzzyProvType(){return this.rightFuzzyProvType;}
        private List<String> initialList;

        public List<String> getInitialList(){return this.initialList;}


        private List<String> fuzzyInitial;

        public List<String> getFuzzyInitial(){return this.fuzzyInitial;}

        private List<String> rightFuzzyInitial;

        public List<String> getRightFuzzyInitial(){return this.rightFuzzyInitial;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyAreaId (List<String> fuzzyAreaId){
            this.fuzzyAreaId = fuzzyAreaId;
            return this;
        }

        public QueryBuilder fuzzyAreaId (String ... fuzzyAreaId){
            this.fuzzyAreaId = solveNullList(fuzzyAreaId);
            return this;
        }

        public QueryBuilder rightFuzzyAreaId (List<String> rightFuzzyAreaId){
            this.rightFuzzyAreaId = rightFuzzyAreaId;
            return this;
        }

        public QueryBuilder rightFuzzyAreaId (String ... rightFuzzyAreaId){
            this.rightFuzzyAreaId = solveNullList(rightFuzzyAreaId);
            return this;
        }

        public QueryBuilder areaId(String areaId){
            setAreaId(areaId);
            return this;
        }

        public QueryBuilder areaIdList(String ... areaId){
            this.areaIdList = solveNullList(areaId);
            return this;
        }

        public QueryBuilder areaIdList(List<String> areaId){
            this.areaIdList = areaId;
            return this;
        }

        public QueryBuilder fetchAreaId(){
            setFetchFields("fetchFields","areaId");
            return this;
        }

        public QueryBuilder excludeAreaId(){
            setFetchFields("excludeFields","areaId");
            return this;
        }

        public QueryBuilder fuzzyAreaCode (List<String> fuzzyAreaCode){
            this.fuzzyAreaCode = fuzzyAreaCode;
            return this;
        }

        public QueryBuilder fuzzyAreaCode (String ... fuzzyAreaCode){
            this.fuzzyAreaCode = solveNullList(fuzzyAreaCode);
            return this;
        }

        public QueryBuilder rightFuzzyAreaCode (List<String> rightFuzzyAreaCode){
            this.rightFuzzyAreaCode = rightFuzzyAreaCode;
            return this;
        }

        public QueryBuilder rightFuzzyAreaCode (String ... rightFuzzyAreaCode){
            this.rightFuzzyAreaCode = solveNullList(rightFuzzyAreaCode);
            return this;
        }

        public QueryBuilder areaCode(String areaCode){
            setAreaCode(areaCode);
            return this;
        }

        public QueryBuilder areaCodeList(String ... areaCode){
            this.areaCodeList = solveNullList(areaCode);
            return this;
        }

        public QueryBuilder areaCodeList(List<String> areaCode){
            this.areaCodeList = areaCode;
            return this;
        }

        public QueryBuilder fetchAreaCode(){
            setFetchFields("fetchFields","areaCode");
            return this;
        }

        public QueryBuilder excludeAreaCode(){
            setFetchFields("excludeFields","areaCode");
            return this;
        }

        public QueryBuilder fuzzyAreaName (List<String> fuzzyAreaName){
            this.fuzzyAreaName = fuzzyAreaName;
            return this;
        }

        public QueryBuilder fuzzyAreaName (String ... fuzzyAreaName){
            this.fuzzyAreaName = solveNullList(fuzzyAreaName);
            return this;
        }

        public QueryBuilder rightFuzzyAreaName (List<String> rightFuzzyAreaName){
            this.rightFuzzyAreaName = rightFuzzyAreaName;
            return this;
        }

        public QueryBuilder rightFuzzyAreaName (String ... rightFuzzyAreaName){
            this.rightFuzzyAreaName = solveNullList(rightFuzzyAreaName);
            return this;
        }

        public QueryBuilder areaName(String areaName){
            setAreaName(areaName);
            return this;
        }

        public QueryBuilder areaNameList(String ... areaName){
            this.areaNameList = solveNullList(areaName);
            return this;
        }

        public QueryBuilder areaNameList(List<String> areaName){
            this.areaNameList = areaName;
            return this;
        }

        public QueryBuilder fetchAreaName(){
            setFetchFields("fetchFields","areaName");
            return this;
        }

        public QueryBuilder excludeAreaName(){
            setFetchFields("excludeFields","areaName");
            return this;
        }

        public QueryBuilder fuzzyLevels (List<String> fuzzyLevels){
            this.fuzzyLevels = fuzzyLevels;
            return this;
        }

        public QueryBuilder fuzzyLevels (String ... fuzzyLevels){
            this.fuzzyLevels = solveNullList(fuzzyLevels);
            return this;
        }

        public QueryBuilder rightFuzzyLevels (List<String> rightFuzzyLevels){
            this.rightFuzzyLevels = rightFuzzyLevels;
            return this;
        }

        public QueryBuilder rightFuzzyLevels (String ... rightFuzzyLevels){
            this.rightFuzzyLevels = solveNullList(rightFuzzyLevels);
            return this;
        }

        public QueryBuilder levels(String levels){
            setLevels(levels);
            return this;
        }

        public QueryBuilder levelsList(String ... levels){
            this.levelsList = solveNullList(levels);
            return this;
        }

        public QueryBuilder levelsList(List<String> levels){
            this.levelsList = levels;
            return this;
        }

        public QueryBuilder fetchLevels(){
            setFetchFields("fetchFields","levels");
            return this;
        }

        public QueryBuilder excludeLevels(){
            setFetchFields("excludeFields","levels");
            return this;
        }

        public QueryBuilder fuzzyLevelOne (List<String> fuzzyLevelOne){
            this.fuzzyLevelOne = fuzzyLevelOne;
            return this;
        }

        public QueryBuilder fuzzyLevelOne (String ... fuzzyLevelOne){
            this.fuzzyLevelOne = solveNullList(fuzzyLevelOne);
            return this;
        }

        public QueryBuilder rightFuzzyLevelOne (List<String> rightFuzzyLevelOne){
            this.rightFuzzyLevelOne = rightFuzzyLevelOne;
            return this;
        }

        public QueryBuilder rightFuzzyLevelOne (String ... rightFuzzyLevelOne){
            this.rightFuzzyLevelOne = solveNullList(rightFuzzyLevelOne);
            return this;
        }

        public QueryBuilder levelOne(String levelOne){
            setLevelOne(levelOne);
            return this;
        }

        public QueryBuilder levelOneList(String ... levelOne){
            this.levelOneList = solveNullList(levelOne);
            return this;
        }

        public QueryBuilder levelOneList(List<String> levelOne){
            this.levelOneList = levelOne;
            return this;
        }

        public QueryBuilder fetchLevelOne(){
            setFetchFields("fetchFields","levelOne");
            return this;
        }

        public QueryBuilder excludeLevelOne(){
            setFetchFields("excludeFields","levelOne");
            return this;
        }

        public QueryBuilder fuzzyLevelTwo (List<String> fuzzyLevelTwo){
            this.fuzzyLevelTwo = fuzzyLevelTwo;
            return this;
        }

        public QueryBuilder fuzzyLevelTwo (String ... fuzzyLevelTwo){
            this.fuzzyLevelTwo = solveNullList(fuzzyLevelTwo);
            return this;
        }

        public QueryBuilder rightFuzzyLevelTwo (List<String> rightFuzzyLevelTwo){
            this.rightFuzzyLevelTwo = rightFuzzyLevelTwo;
            return this;
        }

        public QueryBuilder rightFuzzyLevelTwo (String ... rightFuzzyLevelTwo){
            this.rightFuzzyLevelTwo = solveNullList(rightFuzzyLevelTwo);
            return this;
        }

        public QueryBuilder levelTwo(String levelTwo){
            setLevelTwo(levelTwo);
            return this;
        }

        public QueryBuilder levelTwoList(String ... levelTwo){
            this.levelTwoList = solveNullList(levelTwo);
            return this;
        }

        public QueryBuilder levelTwoList(List<String> levelTwo){
            this.levelTwoList = levelTwo;
            return this;
        }

        public QueryBuilder fetchLevelTwo(){
            setFetchFields("fetchFields","levelTwo");
            return this;
        }

        public QueryBuilder excludeLevelTwo(){
            setFetchFields("excludeFields","levelTwo");
            return this;
        }

        public QueryBuilder fuzzyLevelThree (List<String> fuzzyLevelThree){
            this.fuzzyLevelThree = fuzzyLevelThree;
            return this;
        }

        public QueryBuilder fuzzyLevelThree (String ... fuzzyLevelThree){
            this.fuzzyLevelThree = solveNullList(fuzzyLevelThree);
            return this;
        }

        public QueryBuilder rightFuzzyLevelThree (List<String> rightFuzzyLevelThree){
            this.rightFuzzyLevelThree = rightFuzzyLevelThree;
            return this;
        }

        public QueryBuilder rightFuzzyLevelThree (String ... rightFuzzyLevelThree){
            this.rightFuzzyLevelThree = solveNullList(rightFuzzyLevelThree);
            return this;
        }

        public QueryBuilder levelThree(String levelThree){
            setLevelThree(levelThree);
            return this;
        }

        public QueryBuilder levelThreeList(String ... levelThree){
            this.levelThreeList = solveNullList(levelThree);
            return this;
        }

        public QueryBuilder levelThreeList(List<String> levelThree){
            this.levelThreeList = levelThree;
            return this;
        }

        public QueryBuilder fetchLevelThree(){
            setFetchFields("fetchFields","levelThree");
            return this;
        }

        public QueryBuilder excludeLevelThree(){
            setFetchFields("excludeFields","levelThree");
            return this;
        }

        public QueryBuilder fuzzyLevelFour (List<String> fuzzyLevelFour){
            this.fuzzyLevelFour = fuzzyLevelFour;
            return this;
        }

        public QueryBuilder fuzzyLevelFour (String ... fuzzyLevelFour){
            this.fuzzyLevelFour = solveNullList(fuzzyLevelFour);
            return this;
        }

        public QueryBuilder rightFuzzyLevelFour (List<String> rightFuzzyLevelFour){
            this.rightFuzzyLevelFour = rightFuzzyLevelFour;
            return this;
        }

        public QueryBuilder rightFuzzyLevelFour (String ... rightFuzzyLevelFour){
            this.rightFuzzyLevelFour = solveNullList(rightFuzzyLevelFour);
            return this;
        }

        public QueryBuilder levelFour(String levelFour){
            setLevelFour(levelFour);
            return this;
        }

        public QueryBuilder levelFourList(String ... levelFour){
            this.levelFourList = solveNullList(levelFour);
            return this;
        }

        public QueryBuilder levelFourList(List<String> levelFour){
            this.levelFourList = levelFour;
            return this;
        }

        public QueryBuilder fetchLevelFour(){
            setFetchFields("fetchFields","levelFour");
            return this;
        }

        public QueryBuilder excludeLevelFour(){
            setFetchFields("excludeFields","levelFour");
            return this;
        }

        public QueryBuilder fuzzyAreaStatus (List<String> fuzzyAreaStatus){
            this.fuzzyAreaStatus = fuzzyAreaStatus;
            return this;
        }

        public QueryBuilder fuzzyAreaStatus (String ... fuzzyAreaStatus){
            this.fuzzyAreaStatus = solveNullList(fuzzyAreaStatus);
            return this;
        }

        public QueryBuilder rightFuzzyAreaStatus (List<String> rightFuzzyAreaStatus){
            this.rightFuzzyAreaStatus = rightFuzzyAreaStatus;
            return this;
        }

        public QueryBuilder rightFuzzyAreaStatus (String ... rightFuzzyAreaStatus){
            this.rightFuzzyAreaStatus = solveNullList(rightFuzzyAreaStatus);
            return this;
        }

        public QueryBuilder areaStatus(String areaStatus){
            setAreaStatus(areaStatus);
            return this;
        }

        public QueryBuilder areaStatusList(String ... areaStatus){
            this.areaStatusList = solveNullList(areaStatus);
            return this;
        }

        public QueryBuilder areaStatusList(List<String> areaStatus){
            this.areaStatusList = areaStatus;
            return this;
        }

        public QueryBuilder fetchAreaStatus(){
            setFetchFields("fetchFields","areaStatus");
            return this;
        }

        public QueryBuilder excludeAreaStatus(){
            setFetchFields("excludeFields","areaStatus");
            return this;
        }

        public QueryBuilder fuzzyLatitude (List<String> fuzzyLatitude){
            this.fuzzyLatitude = fuzzyLatitude;
            return this;
        }

        public QueryBuilder fuzzyLatitude (String ... fuzzyLatitude){
            this.fuzzyLatitude = solveNullList(fuzzyLatitude);
            return this;
        }

        public QueryBuilder rightFuzzyLatitude (List<String> rightFuzzyLatitude){
            this.rightFuzzyLatitude = rightFuzzyLatitude;
            return this;
        }

        public QueryBuilder rightFuzzyLatitude (String ... rightFuzzyLatitude){
            this.rightFuzzyLatitude = solveNullList(rightFuzzyLatitude);
            return this;
        }

        public QueryBuilder latitude(String latitude){
            setLatitude(latitude);
            return this;
        }

        public QueryBuilder latitudeList(String ... latitude){
            this.latitudeList = solveNullList(latitude);
            return this;
        }

        public QueryBuilder latitudeList(List<String> latitude){
            this.latitudeList = latitude;
            return this;
        }

        public QueryBuilder fetchLatitude(){
            setFetchFields("fetchFields","latitude");
            return this;
        }

        public QueryBuilder excludeLatitude(){
            setFetchFields("excludeFields","latitude");
            return this;
        }

        public QueryBuilder fuzzyLongitude (List<String> fuzzyLongitude){
            this.fuzzyLongitude = fuzzyLongitude;
            return this;
        }

        public QueryBuilder fuzzyLongitude (String ... fuzzyLongitude){
            this.fuzzyLongitude = solveNullList(fuzzyLongitude);
            return this;
        }

        public QueryBuilder rightFuzzyLongitude (List<String> rightFuzzyLongitude){
            this.rightFuzzyLongitude = rightFuzzyLongitude;
            return this;
        }

        public QueryBuilder rightFuzzyLongitude (String ... rightFuzzyLongitude){
            this.rightFuzzyLongitude = solveNullList(rightFuzzyLongitude);
            return this;
        }

        public QueryBuilder longitude(String longitude){
            setLongitude(longitude);
            return this;
        }

        public QueryBuilder longitudeList(String ... longitude){
            this.longitudeList = solveNullList(longitude);
            return this;
        }

        public QueryBuilder longitudeList(List<String> longitude){
            this.longitudeList = longitude;
            return this;
        }

        public QueryBuilder fetchLongitude(){
            setFetchFields("fetchFields","longitude");
            return this;
        }

        public QueryBuilder excludeLongitude(){
            setFetchFields("excludeFields","longitude");
            return this;
        }

        public QueryBuilder fuzzyProvType (List<String> fuzzyProvType){
            this.fuzzyProvType = fuzzyProvType;
            return this;
        }

        public QueryBuilder fuzzyProvType (String ... fuzzyProvType){
            this.fuzzyProvType = solveNullList(fuzzyProvType);
            return this;
        }

        public QueryBuilder rightFuzzyProvType (List<String> rightFuzzyProvType){
            this.rightFuzzyProvType = rightFuzzyProvType;
            return this;
        }

        public QueryBuilder rightFuzzyProvType (String ... rightFuzzyProvType){
            this.rightFuzzyProvType = solveNullList(rightFuzzyProvType);
            return this;
        }

        public QueryBuilder provType(String provType){
            setProvType(provType);
            return this;
        }

        public QueryBuilder provTypeList(String ... provType){
            this.provTypeList = solveNullList(provType);
            return this;
        }

        public QueryBuilder provTypeList(List<String> provType){
            this.provTypeList = provType;
            return this;
        }

        public QueryBuilder fetchProvType(){
            setFetchFields("fetchFields","provType");
            return this;
        }

        public QueryBuilder excludeProvType(){
            setFetchFields("excludeFields","provType");
            return this;
        }

        public QueryBuilder fuzzyInitial (List<String> fuzzyInitial){
            this.fuzzyInitial = fuzzyInitial;
            return this;
        }

        public QueryBuilder fuzzyInitial (String ... fuzzyInitial){
            this.fuzzyInitial = solveNullList(fuzzyInitial);
            return this;
        }

        public QueryBuilder rightFuzzyInitial (List<String> rightFuzzyInitial){
            this.rightFuzzyInitial = rightFuzzyInitial;
            return this;
        }

        public QueryBuilder rightFuzzyInitial (String ... rightFuzzyInitial){
            this.rightFuzzyInitial = solveNullList(rightFuzzyInitial);
            return this;
        }

        public QueryBuilder initial(String initial){
            setInitial(initial);
            return this;
        }

        public QueryBuilder initialList(String ... initial){
            this.initialList = solveNullList(initial);
            return this;
        }

        public QueryBuilder initialList(List<String> initial){
            this.initialList = initial;
            return this;
        }

        public QueryBuilder fetchInitial(){
            setFetchFields("fetchFields","initial");
            return this;
        }

        public QueryBuilder excludeInitial(){
            setFetchFields("excludeFields","initial");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public SysArea build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> areaIdList;

        public List<String> getAreaIdList(){return this.areaIdList;}


        private List<String> fuzzyAreaId;

        public List<String> getFuzzyAreaId(){return this.fuzzyAreaId;}

        private List<String> rightFuzzyAreaId;

        public List<String> getRightFuzzyAreaId(){return this.rightFuzzyAreaId;}
        private List<String> areaCodeList;

        public List<String> getAreaCodeList(){return this.areaCodeList;}


        private List<String> fuzzyAreaCode;

        public List<String> getFuzzyAreaCode(){return this.fuzzyAreaCode;}

        private List<String> rightFuzzyAreaCode;

        public List<String> getRightFuzzyAreaCode(){return this.rightFuzzyAreaCode;}
        private List<String> areaNameList;

        public List<String> getAreaNameList(){return this.areaNameList;}


        private List<String> fuzzyAreaName;

        public List<String> getFuzzyAreaName(){return this.fuzzyAreaName;}

        private List<String> rightFuzzyAreaName;

        public List<String> getRightFuzzyAreaName(){return this.rightFuzzyAreaName;}
        private List<String> levelsList;

        public List<String> getLevelsList(){return this.levelsList;}


        private List<String> fuzzyLevels;

        public List<String> getFuzzyLevels(){return this.fuzzyLevels;}

        private List<String> rightFuzzyLevels;

        public List<String> getRightFuzzyLevels(){return this.rightFuzzyLevels;}
        private List<String> levelOneList;

        public List<String> getLevelOneList(){return this.levelOneList;}


        private List<String> fuzzyLevelOne;

        public List<String> getFuzzyLevelOne(){return this.fuzzyLevelOne;}

        private List<String> rightFuzzyLevelOne;

        public List<String> getRightFuzzyLevelOne(){return this.rightFuzzyLevelOne;}
        private List<String> levelTwoList;

        public List<String> getLevelTwoList(){return this.levelTwoList;}


        private List<String> fuzzyLevelTwo;

        public List<String> getFuzzyLevelTwo(){return this.fuzzyLevelTwo;}

        private List<String> rightFuzzyLevelTwo;

        public List<String> getRightFuzzyLevelTwo(){return this.rightFuzzyLevelTwo;}
        private List<String> levelThreeList;

        public List<String> getLevelThreeList(){return this.levelThreeList;}


        private List<String> fuzzyLevelThree;

        public List<String> getFuzzyLevelThree(){return this.fuzzyLevelThree;}

        private List<String> rightFuzzyLevelThree;

        public List<String> getRightFuzzyLevelThree(){return this.rightFuzzyLevelThree;}
        private List<String> levelFourList;

        public List<String> getLevelFourList(){return this.levelFourList;}


        private List<String> fuzzyLevelFour;

        public List<String> getFuzzyLevelFour(){return this.fuzzyLevelFour;}

        private List<String> rightFuzzyLevelFour;

        public List<String> getRightFuzzyLevelFour(){return this.rightFuzzyLevelFour;}
        private List<String> areaStatusList;

        public List<String> getAreaStatusList(){return this.areaStatusList;}


        private List<String> fuzzyAreaStatus;

        public List<String> getFuzzyAreaStatus(){return this.fuzzyAreaStatus;}

        private List<String> rightFuzzyAreaStatus;

        public List<String> getRightFuzzyAreaStatus(){return this.rightFuzzyAreaStatus;}
        private List<String> latitudeList;

        public List<String> getLatitudeList(){return this.latitudeList;}


        private List<String> fuzzyLatitude;

        public List<String> getFuzzyLatitude(){return this.fuzzyLatitude;}

        private List<String> rightFuzzyLatitude;

        public List<String> getRightFuzzyLatitude(){return this.rightFuzzyLatitude;}
        private List<String> longitudeList;

        public List<String> getLongitudeList(){return this.longitudeList;}


        private List<String> fuzzyLongitude;

        public List<String> getFuzzyLongitude(){return this.fuzzyLongitude;}

        private List<String> rightFuzzyLongitude;

        public List<String> getRightFuzzyLongitude(){return this.rightFuzzyLongitude;}
        private List<String> provTypeList;

        public List<String> getProvTypeList(){return this.provTypeList;}


        private List<String> fuzzyProvType;

        public List<String> getFuzzyProvType(){return this.fuzzyProvType;}

        private List<String> rightFuzzyProvType;

        public List<String> getRightFuzzyProvType(){return this.rightFuzzyProvType;}
        private List<String> initialList;

        public List<String> getInitialList(){return this.initialList;}


        private List<String> fuzzyInitial;

        public List<String> getFuzzyInitial(){return this.fuzzyInitial;}

        private List<String> rightFuzzyInitial;

        public List<String> getRightFuzzyInitial(){return this.rightFuzzyInitial;}

        public ConditionBuilder fuzzyAreaId (List<String> fuzzyAreaId){
            this.fuzzyAreaId = fuzzyAreaId;
            return this;
        }

        public ConditionBuilder fuzzyAreaId (String ... fuzzyAreaId){
            this.fuzzyAreaId = solveNullList(fuzzyAreaId);
            return this;
        }

        public ConditionBuilder rightFuzzyAreaId (List<String> rightFuzzyAreaId){
            this.rightFuzzyAreaId = rightFuzzyAreaId;
            return this;
        }

        public ConditionBuilder rightFuzzyAreaId (String ... rightFuzzyAreaId){
            this.rightFuzzyAreaId = solveNullList(rightFuzzyAreaId);
            return this;
        }

        public ConditionBuilder areaIdList(String ... areaId){
            this.areaIdList = solveNullList(areaId);
            return this;
        }

        public ConditionBuilder areaIdList(List<String> areaId){
            this.areaIdList = areaId;
            return this;
        }

        public ConditionBuilder fuzzyAreaCode (List<String> fuzzyAreaCode){
            this.fuzzyAreaCode = fuzzyAreaCode;
            return this;
        }

        public ConditionBuilder fuzzyAreaCode (String ... fuzzyAreaCode){
            this.fuzzyAreaCode = solveNullList(fuzzyAreaCode);
            return this;
        }

        public ConditionBuilder rightFuzzyAreaCode (List<String> rightFuzzyAreaCode){
            this.rightFuzzyAreaCode = rightFuzzyAreaCode;
            return this;
        }

        public ConditionBuilder rightFuzzyAreaCode (String ... rightFuzzyAreaCode){
            this.rightFuzzyAreaCode = solveNullList(rightFuzzyAreaCode);
            return this;
        }

        public ConditionBuilder areaCodeList(String ... areaCode){
            this.areaCodeList = solveNullList(areaCode);
            return this;
        }

        public ConditionBuilder areaCodeList(List<String> areaCode){
            this.areaCodeList = areaCode;
            return this;
        }

        public ConditionBuilder fuzzyAreaName (List<String> fuzzyAreaName){
            this.fuzzyAreaName = fuzzyAreaName;
            return this;
        }

        public ConditionBuilder fuzzyAreaName (String ... fuzzyAreaName){
            this.fuzzyAreaName = solveNullList(fuzzyAreaName);
            return this;
        }

        public ConditionBuilder rightFuzzyAreaName (List<String> rightFuzzyAreaName){
            this.rightFuzzyAreaName = rightFuzzyAreaName;
            return this;
        }

        public ConditionBuilder rightFuzzyAreaName (String ... rightFuzzyAreaName){
            this.rightFuzzyAreaName = solveNullList(rightFuzzyAreaName);
            return this;
        }

        public ConditionBuilder areaNameList(String ... areaName){
            this.areaNameList = solveNullList(areaName);
            return this;
        }

        public ConditionBuilder areaNameList(List<String> areaName){
            this.areaNameList = areaName;
            return this;
        }

        public ConditionBuilder fuzzyLevels (List<String> fuzzyLevels){
            this.fuzzyLevels = fuzzyLevels;
            return this;
        }

        public ConditionBuilder fuzzyLevels (String ... fuzzyLevels){
            this.fuzzyLevels = solveNullList(fuzzyLevels);
            return this;
        }

        public ConditionBuilder rightFuzzyLevels (List<String> rightFuzzyLevels){
            this.rightFuzzyLevels = rightFuzzyLevels;
            return this;
        }

        public ConditionBuilder rightFuzzyLevels (String ... rightFuzzyLevels){
            this.rightFuzzyLevels = solveNullList(rightFuzzyLevels);
            return this;
        }

        public ConditionBuilder levelsList(String ... levels){
            this.levelsList = solveNullList(levels);
            return this;
        }

        public ConditionBuilder levelsList(List<String> levels){
            this.levelsList = levels;
            return this;
        }

        public ConditionBuilder fuzzyLevelOne (List<String> fuzzyLevelOne){
            this.fuzzyLevelOne = fuzzyLevelOne;
            return this;
        }

        public ConditionBuilder fuzzyLevelOne (String ... fuzzyLevelOne){
            this.fuzzyLevelOne = solveNullList(fuzzyLevelOne);
            return this;
        }

        public ConditionBuilder rightFuzzyLevelOne (List<String> rightFuzzyLevelOne){
            this.rightFuzzyLevelOne = rightFuzzyLevelOne;
            return this;
        }

        public ConditionBuilder rightFuzzyLevelOne (String ... rightFuzzyLevelOne){
            this.rightFuzzyLevelOne = solveNullList(rightFuzzyLevelOne);
            return this;
        }

        public ConditionBuilder levelOneList(String ... levelOne){
            this.levelOneList = solveNullList(levelOne);
            return this;
        }

        public ConditionBuilder levelOneList(List<String> levelOne){
            this.levelOneList = levelOne;
            return this;
        }

        public ConditionBuilder fuzzyLevelTwo (List<String> fuzzyLevelTwo){
            this.fuzzyLevelTwo = fuzzyLevelTwo;
            return this;
        }

        public ConditionBuilder fuzzyLevelTwo (String ... fuzzyLevelTwo){
            this.fuzzyLevelTwo = solveNullList(fuzzyLevelTwo);
            return this;
        }

        public ConditionBuilder rightFuzzyLevelTwo (List<String> rightFuzzyLevelTwo){
            this.rightFuzzyLevelTwo = rightFuzzyLevelTwo;
            return this;
        }

        public ConditionBuilder rightFuzzyLevelTwo (String ... rightFuzzyLevelTwo){
            this.rightFuzzyLevelTwo = solveNullList(rightFuzzyLevelTwo);
            return this;
        }

        public ConditionBuilder levelTwoList(String ... levelTwo){
            this.levelTwoList = solveNullList(levelTwo);
            return this;
        }

        public ConditionBuilder levelTwoList(List<String> levelTwo){
            this.levelTwoList = levelTwo;
            return this;
        }

        public ConditionBuilder fuzzyLevelThree (List<String> fuzzyLevelThree){
            this.fuzzyLevelThree = fuzzyLevelThree;
            return this;
        }

        public ConditionBuilder fuzzyLevelThree (String ... fuzzyLevelThree){
            this.fuzzyLevelThree = solveNullList(fuzzyLevelThree);
            return this;
        }

        public ConditionBuilder rightFuzzyLevelThree (List<String> rightFuzzyLevelThree){
            this.rightFuzzyLevelThree = rightFuzzyLevelThree;
            return this;
        }

        public ConditionBuilder rightFuzzyLevelThree (String ... rightFuzzyLevelThree){
            this.rightFuzzyLevelThree = solveNullList(rightFuzzyLevelThree);
            return this;
        }

        public ConditionBuilder levelThreeList(String ... levelThree){
            this.levelThreeList = solveNullList(levelThree);
            return this;
        }

        public ConditionBuilder levelThreeList(List<String> levelThree){
            this.levelThreeList = levelThree;
            return this;
        }

        public ConditionBuilder fuzzyLevelFour (List<String> fuzzyLevelFour){
            this.fuzzyLevelFour = fuzzyLevelFour;
            return this;
        }

        public ConditionBuilder fuzzyLevelFour (String ... fuzzyLevelFour){
            this.fuzzyLevelFour = solveNullList(fuzzyLevelFour);
            return this;
        }

        public ConditionBuilder rightFuzzyLevelFour (List<String> rightFuzzyLevelFour){
            this.rightFuzzyLevelFour = rightFuzzyLevelFour;
            return this;
        }

        public ConditionBuilder rightFuzzyLevelFour (String ... rightFuzzyLevelFour){
            this.rightFuzzyLevelFour = solveNullList(rightFuzzyLevelFour);
            return this;
        }

        public ConditionBuilder levelFourList(String ... levelFour){
            this.levelFourList = solveNullList(levelFour);
            return this;
        }

        public ConditionBuilder levelFourList(List<String> levelFour){
            this.levelFourList = levelFour;
            return this;
        }

        public ConditionBuilder fuzzyAreaStatus (List<String> fuzzyAreaStatus){
            this.fuzzyAreaStatus = fuzzyAreaStatus;
            return this;
        }

        public ConditionBuilder fuzzyAreaStatus (String ... fuzzyAreaStatus){
            this.fuzzyAreaStatus = solveNullList(fuzzyAreaStatus);
            return this;
        }

        public ConditionBuilder rightFuzzyAreaStatus (List<String> rightFuzzyAreaStatus){
            this.rightFuzzyAreaStatus = rightFuzzyAreaStatus;
            return this;
        }

        public ConditionBuilder rightFuzzyAreaStatus (String ... rightFuzzyAreaStatus){
            this.rightFuzzyAreaStatus = solveNullList(rightFuzzyAreaStatus);
            return this;
        }

        public ConditionBuilder areaStatusList(String ... areaStatus){
            this.areaStatusList = solveNullList(areaStatus);
            return this;
        }

        public ConditionBuilder areaStatusList(List<String> areaStatus){
            this.areaStatusList = areaStatus;
            return this;
        }

        public ConditionBuilder fuzzyLatitude (List<String> fuzzyLatitude){
            this.fuzzyLatitude = fuzzyLatitude;
            return this;
        }

        public ConditionBuilder fuzzyLatitude (String ... fuzzyLatitude){
            this.fuzzyLatitude = solveNullList(fuzzyLatitude);
            return this;
        }

        public ConditionBuilder rightFuzzyLatitude (List<String> rightFuzzyLatitude){
            this.rightFuzzyLatitude = rightFuzzyLatitude;
            return this;
        }

        public ConditionBuilder rightFuzzyLatitude (String ... rightFuzzyLatitude){
            this.rightFuzzyLatitude = solveNullList(rightFuzzyLatitude);
            return this;
        }

        public ConditionBuilder latitudeList(String ... latitude){
            this.latitudeList = solveNullList(latitude);
            return this;
        }

        public ConditionBuilder latitudeList(List<String> latitude){
            this.latitudeList = latitude;
            return this;
        }

        public ConditionBuilder fuzzyLongitude (List<String> fuzzyLongitude){
            this.fuzzyLongitude = fuzzyLongitude;
            return this;
        }

        public ConditionBuilder fuzzyLongitude (String ... fuzzyLongitude){
            this.fuzzyLongitude = solveNullList(fuzzyLongitude);
            return this;
        }

        public ConditionBuilder rightFuzzyLongitude (List<String> rightFuzzyLongitude){
            this.rightFuzzyLongitude = rightFuzzyLongitude;
            return this;
        }

        public ConditionBuilder rightFuzzyLongitude (String ... rightFuzzyLongitude){
            this.rightFuzzyLongitude = solveNullList(rightFuzzyLongitude);
            return this;
        }

        public ConditionBuilder longitudeList(String ... longitude){
            this.longitudeList = solveNullList(longitude);
            return this;
        }

        public ConditionBuilder longitudeList(List<String> longitude){
            this.longitudeList = longitude;
            return this;
        }

        public ConditionBuilder fuzzyProvType (List<String> fuzzyProvType){
            this.fuzzyProvType = fuzzyProvType;
            return this;
        }

        public ConditionBuilder fuzzyProvType (String ... fuzzyProvType){
            this.fuzzyProvType = solveNullList(fuzzyProvType);
            return this;
        }

        public ConditionBuilder rightFuzzyProvType (List<String> rightFuzzyProvType){
            this.rightFuzzyProvType = rightFuzzyProvType;
            return this;
        }

        public ConditionBuilder rightFuzzyProvType (String ... rightFuzzyProvType){
            this.rightFuzzyProvType = solveNullList(rightFuzzyProvType);
            return this;
        }

        public ConditionBuilder provTypeList(String ... provType){
            this.provTypeList = solveNullList(provType);
            return this;
        }

        public ConditionBuilder provTypeList(List<String> provType){
            this.provTypeList = provType;
            return this;
        }

        public ConditionBuilder fuzzyInitial (List<String> fuzzyInitial){
            this.fuzzyInitial = fuzzyInitial;
            return this;
        }

        public ConditionBuilder fuzzyInitial (String ... fuzzyInitial){
            this.fuzzyInitial = solveNullList(fuzzyInitial);
            return this;
        }

        public ConditionBuilder rightFuzzyInitial (List<String> rightFuzzyInitial){
            this.rightFuzzyInitial = rightFuzzyInitial;
            return this;
        }

        public ConditionBuilder rightFuzzyInitial (String ... rightFuzzyInitial){
            this.rightFuzzyInitial = solveNullList(rightFuzzyInitial);
            return this;
        }

        public ConditionBuilder initialList(String ... initial){
            this.initialList = solveNullList(initial);
            return this;
        }

        public ConditionBuilder initialList(List<String> initial){
            this.initialList = initial;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private SysArea obj;

        public Builder(){
            this.obj = new SysArea();
        }

        public Builder areaId(String areaId){
            this.obj.setAreaId(areaId);
            return this;
        }
        public Builder areaCode(String areaCode){
            this.obj.setAreaCode(areaCode);
            return this;
        }
        public Builder areaName(String areaName){
            this.obj.setAreaName(areaName);
            return this;
        }
        public Builder levels(String levels){
            this.obj.setLevels(levels);
            return this;
        }
        public Builder levelOne(String levelOne){
            this.obj.setLevelOne(levelOne);
            return this;
        }
        public Builder levelTwo(String levelTwo){
            this.obj.setLevelTwo(levelTwo);
            return this;
        }
        public Builder levelThree(String levelThree){
            this.obj.setLevelThree(levelThree);
            return this;
        }
        public Builder levelFour(String levelFour){
            this.obj.setLevelFour(levelFour);
            return this;
        }
        public Builder areaStatus(String areaStatus){
            this.obj.setAreaStatus(areaStatus);
            return this;
        }
        public Builder latitude(String latitude){
            this.obj.setLatitude(latitude);
            return this;
        }
        public Builder longitude(String longitude){
            this.obj.setLongitude(longitude);
            return this;
        }
        public Builder provType(String provType){
            this.obj.setProvType(provType);
            return this;
        }
        public Builder initial(String initial){
            this.obj.setInitial(initial);
            return this;
        }
        public SysArea build(){return obj;}
    }

}
