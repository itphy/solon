package com.itphy.test;

import com.itphy.mapper.UserInfoMapper;
import com.itphy.plus.Page;
import com.itphy.pojo.UserInfo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class TestInvocationHandler implements InvocationHandler {

    private Object obj;

    public TestInvocationHandler(Object o){
        this.obj=o;
    }

    public static void main(String[] args) {
        UserInfoMapper user = new UserInfoMapper() {
            @Override
            public int insertUserInfo(UserInfo object) {
                return 0;
            }

            @Override
            public int updateUserInfo(UserInfo object) {
                return 0;
            }

            @Override
            public int update(UserInfo.UpdateBuilder object) {
                return 0;
            }

            @Override
            public Page<UserInfo> queryUserInfo(UserInfo object) {
                return null;
            }

            @Override
            public UserInfo queryUserInfoLimit1(UserInfo object) {
                return null;
            }
        };
        InvocationHandler handler  = new TestInvocationHandler(user);
        UserInfoMapper proxy = (UserInfoMapper) Proxy.newProxyInstance(handler.getClass().getClassLoader(), user.getClass().getInterfaces(), handler);
        System.out.println(proxy.queryUserInfo(null));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(obj, args);
    }

}
