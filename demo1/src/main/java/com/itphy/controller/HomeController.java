package com.itphy.controller;

import com.itphy.DescLog;
import com.itphy.mapper.UserInfoMapper;
import com.itphy.plus.PageHelper;
import com.itphy.pojo.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Controller
public class HomeController {

    @Resource
    private UserInfoMapper userInfoMapper;

    @DescLog(text = "分页查询数据")
    @GetMapping("/al")
    @CrossOrigin
    @ResponseBody
    public Object getData(Integer pageNum,Integer pageSize){
        PageHelper.openLimit(pageNum, pageSize);
        PageHelper.openGroups("desc");
        return userInfoMapper.queryUserInfo(new UserInfo());
    }

    @GetMapping("/toLogin")
    public String toLogin(){
        return "login";
    }

    @GetMapping("/go/{id}")
    public String go(@PathVariable("id") String id, Model model){
        model.addAttribute("id",id);
        return "go";
    }
}
