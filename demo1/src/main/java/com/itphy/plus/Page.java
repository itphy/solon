package com.itphy.plus;

import java.util.List;

public class Page<T> {
    private Integer total;//总条数
    private Integer size;//当前条数
    private Integer pages;//总页数
    private Integer pageNum;//当前第几页
    private Integer pageSize;//每页条数
    private List<T> list;

    public Integer getTotal() {
        return total;
    }

    public Page setTotal(Integer total) {
        this.total = total;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public Page setSize(Integer size) {
        this.size = size;
        return this;
    }

    public Integer getPages() {
        return pages;
    }

    public Page setPages(Integer pages) {
        this.pages = pages;
        return this;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public Page setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
        return this;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public Page setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public List<T> getList() {
        return list;
    }

    public Page setList(List<T> list) {
        this.list = list;
        return this;
    }

    public Page build(List<T> list){
        this.list=list;
        this.size=list.size();
        this.pages=(total/pageSize)+(total%pageSize==0?0:1);
        this.pageNum=(pageNum>pages?pages:pageNum);
        return this;
    }
}
