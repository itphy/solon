package com.itphy.plus;

import java.util.List;
import java.util.stream.Collectors;

public class OrderByParam {
    private String name;
    private Integer sort;
    private Integer priority;
    private String description;
    private String[] groups;

    public String[] getGroups() {
        return groups;
    }

    public OrderByParam setGroups(String... groups) {
        this.groups = groups;
        return this;
    }

    public String getName() {
        return name;
    }

    public OrderByParam setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getSort() {
        return sort;
    }

    public OrderByParam setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    public Integer getPriority() {
        return priority;
    }

    public OrderByParam setPriority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public OrderByParam setDescription(String description) {
        this.description = description;
        return this;
    }

    public static String renderOrderBy(List<OrderByParam> orderByParams){
        StringBuilder sb=new StringBuilder(" ORDER BY ");
        orderByParams.stream().sorted((t1,t2)->{
            return t1.getPriority()-t2.getPriority();
        }).collect(Collectors.toList());

        for (OrderByParam orderByParam : orderByParams) {
            sb.append(orderByParam.getName()+" ");
            if(orderByParam.getSort()==0){
                sb.append(" DESC,");
            }
            if(orderByParam.getSort()==1){
                sb.append(" ASC,");
            }
        }
        return sb.substring(0, sb.length()-1);
    }
}
