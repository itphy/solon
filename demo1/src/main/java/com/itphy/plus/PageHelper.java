package com.itphy.plus;

import java.util.Objects;

public class PageHelper {
    private static ThreadLocal<Page> params=new ThreadLocal<>();
    private static ThreadLocal<String> orderBy=new ThreadLocal<>();

    public static void openLimit(Integer pageNum,Integer pageSize){
        params.set(new Page().setPageNum((Objects.isNull(pageNum)||pageNum<=0)?1:pageNum).setPageSize((Objects.isNull(pageSize)||pageSize<=0)?10:pageSize));
    }

    public static Page getPage(){
        Page page = params.get();
        if(Objects.isNull(page)){
            openLimit(1,10);
            page = params.get();
        }
        params.remove();
        return page;
    }

    public static void openGroups(String group){
        orderBy.set(group);
    }

    public static String getGroups(){
        String group = orderBy.get();
        orderBy.remove();
        return group;
    }
}
