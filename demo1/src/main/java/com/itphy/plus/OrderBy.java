package com.itphy.plus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OrderBy {
    int priority() default 0; //排序优先级
    int sort() default 0;//0 降序  1 升序
    String description() default "";//描述
    String[] groups() default {};//分组
}
