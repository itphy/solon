package com.itphy.mapper.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.itphy.pojo.SysArea;
/**
*  @author author
*/
public interface SysAreaBaseMapper {

    int insertSysArea(SysArea object);

    int updateSysArea(SysArea object);

    int update(SysArea.UpdateBuilder object);

    List<SysArea> querySysArea(SysArea object);

    SysArea querySysAreaLimit1(SysArea object);

}