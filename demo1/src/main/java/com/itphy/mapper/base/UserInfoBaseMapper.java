package com.itphy.mapper.base;

import java.util.List;

import com.itphy.DescLog;
import com.itphy.plus.Page;
import org.apache.ibatis.annotations.Param;
import com.itphy.pojo.UserInfo;
/**
*  @author author
*/
public interface UserInfoBaseMapper {

    int insertUserInfo(UserInfo object);

    int updateUserInfo(UserInfo object);

    int update(UserInfo.UpdateBuilder object);

    Page<UserInfo> queryUserInfo(UserInfo object);

    UserInfo queryUserInfoLimit1(UserInfo object);

}