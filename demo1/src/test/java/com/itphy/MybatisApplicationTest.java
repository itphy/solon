package com.itphy;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.itphy.mapper.SysAreaMapper;
import com.itphy.mapper.UserInfoMapper;
import com.itphy.pojo.SysArea;
import com.itphy.util.JSONTree;
import org.assertj.core.internal.Maps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MybatisApplicationTest {

    @Resource
    RabbitTemplate rabbitTemplate;
    @Resource
    SysAreaMapper sysAreaMapper;
    @Resource
    RedisTemplate redisTemplate;

    @Test
    public void demo4(){
        System.out.println("11".equals(new Integer(11).toString()));
    }

    @Test
    public void demo1(){
//        MessageProperties messageProperties = new MessageProperties();
//        messageProperties.setExpiration("5000");
//        Message message = new Message("单条消息設置".getBytes(StandardCharsets.UTF_8), messageProperties);
//        rabbitTemplate.convertAndSend("all", message);

        rabbitTemplate.convertAndSend("testDirectExchange","route1","111");

//        rabbitTemplate.convertAndSend("testDirectExchange", "route1",
//                JSON.toJSONString(Maps.instance()),msg -> {
//                    msg.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
//                    // 模拟，设置5S后消息过期
//                    msg.getMessageProperties().setExpiration("5000");
//                    return msg;
//                });

    }

    @Test
    public void demo2(){
        long l = System.currentTimeMillis();
        SysArea sysArea = new SysArea();
        JSONTree tree=new JSONTree();
        sysArea.setLevels("1");
        List<SysArea> province = sysAreaMapper.querySysArea(sysArea);
        for (SysArea p : province) {
            tree.put(p);
        }
        sysArea.setLevels("2");
        List<SysArea> city = sysAreaMapper.querySysArea(sysArea);
        for (SysArea p : city) {
            tree.put(p);
        }
        sysArea.setLevels("3");
        List<SysArea> area = sysAreaMapper.querySysArea(sysArea);
        for (SysArea p : area) {
            tree.put(p);
        }
        long f = System.currentTimeMillis();
        System.out.println(tree.toJsonString());
        System.out.println("用时：：：》》》》》"+(f-l));
    }

    @Test
    public void demo3(){

    }

    private JSONArray getChildren(SysArea sysArea){
        if(Objects.isNull(sysArea) || Objects.isNull(sysArea.getLevelOne()) || Objects.isNull(sysArea.getLevels())){
            sysArea=new SysArea();
            sysArea.setLevels("1");
        }
        JSONArray childArray=new JSONArray();
        List<SysArea> data = sysAreaMapper.querySysArea(sysArea);
        for (SysArea t : data) {
            JSONObject childObj=new JSONObject();
            childObj.put("label", t.getAreaName());
            childObj.put("value", t.getAreaCode());
            try {
                childObj.put("children", getChildren(levelParams(t)));
            }catch (Exception e){
                childObj.put("children", Arrays.asList());
            }
            childArray.add(childObj);
        }
        return childArray;
    }

    private SysArea levelParams(SysArea sysArea){
        if(Objects.isNull(sysArea) || StringUtils.isEmpty(sysArea.getLevelOne())){
            throw new RuntimeException("参数异常");
        }
        SysArea params = new SysArea();
        if(Integer.parseInt(sysArea.getLevelOne())!=0){
            params.setLevelOne(sysArea.getLevelOne());
            params.setLevels("2");
        }

        if(Integer.parseInt(sysArea.getLevelTwo())!=0){
            params.setLevelTwo(sysArea.getLevelTwo());
            params.setLevels("3");
        }

        if(Integer.parseInt(sysArea.getLevelThree())!=0){
            throw new RuntimeException("已经是最后一级");
        }

        return params;
    }



}
