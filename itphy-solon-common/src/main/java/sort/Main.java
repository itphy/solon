package sort;

import java.lang.reflect.Field;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {
    public static void main(String[] args) {
        Class<AnnoationTest> c = AnnoationTest.class;
        Object annoationTest = new AnnoationTest();
        Class<?> aClass = annoationTest.getClass();
        for (Field f : c.getDeclaredFields()) {
            // 判断这个字段是否有MyField注解
            if(f.isAnnotationPresent(Test.class)){
                Test a = f.getAnnotation(Test.class);
                System.out.println(f.getName());
                System.out.println(a.max());
                System.out.println(a.min());
                System.out.println(a.description());
            }
        }
    }
}


class Run {
    public static void main(String args[]){
        Runnable runnable = new RunnableDemo();
        (RunnableDemo.thread=new Thread(runnable)).start();
    }
}