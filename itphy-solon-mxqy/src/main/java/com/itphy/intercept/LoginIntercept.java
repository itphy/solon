package com.itphy.intercept;

import com.itphy.util.JwtUtils;
import org.noear.solon.core.BeanBuilder;
import org.noear.solon.core.BeanWrap;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Handler;

import java.lang.annotation.Annotation;
import java.util.Objects;

public class LoginIntercept implements Handler {
    @Override
    public void handle(Context ctx) throws Throwable {
        String token = ctx.header("token");
        if(Objects.isNull(token) || Objects.isNull(JwtUtils.checkJWT(token))){
            ctx.setHandled(false);
        }
        ctx.setHandled(true);
    }
}
