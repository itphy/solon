package com.itphy;

import com.zaxxer.hikari.HikariDataSource;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Objects;

@Configuration
public class MyConfig {

    @Bean
    public HikariDataSource db1(@Inject("${datasource.db1}") HikariDataSource ds) {
        return ds;
    }



}
