package com.itphy.common;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class R implements Serializable {
    private Integer code;
    private Boolean status;
    private String msg;
    private Object data;
    private R(){}
    private R(Map<String,Object> data){
        this.data=data;
    }
    public static R ok(){
    return new R(new HashMap<>()).code(200).msg("请求成功！").status(true);
    }
    public static R fail(){
        return new R(new HashMap<>()).code(500).msg("系统繁忙！").status(false);
    }
    public R code(Integer code){
        this.code=code;
        return this;
    }
    public R msg(String msg){
        this.msg=msg;
        return this;
    }
    public R data(String k,Object v){
        ((Map<String,Object>)this.data).put(k,v);
        return this;
    }
    public R data(Object data){
        this.data=data;
        return this;
    }
    public R status(Boolean status){
        this.status=status;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
