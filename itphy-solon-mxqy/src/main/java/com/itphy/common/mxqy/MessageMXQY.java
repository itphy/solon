package com.itphy.common.mxqy;

import lombok.Data;

@Data
public class MessageMXQY {
    private String content;  //发送内容
    private String sendFrom; //发送者
    private String sendToForm; //发送给
    private Integer type; //发送类型
}
