package com.itphy.common.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class WechatParam {
    private String signature;
    private String timestamp;
    private String nonce;
    private String echostr;
}
