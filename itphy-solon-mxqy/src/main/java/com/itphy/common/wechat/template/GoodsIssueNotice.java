package com.itphy.common.wechat.template;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 发货通知单
 */
@Data
@Accessors(chain = true)
public class GoodsIssueNotice {
    public static final String KEY="ViEL1vrubQ-jPyBhoTWKUQdzlHB1MfheCUuRc1Zq37Q";
    private Long   bussId;
    private String openId;
    private String company;
    private String warehouse;
    private String orderCode;
    private String pdfUrl;
    private String time;
}
