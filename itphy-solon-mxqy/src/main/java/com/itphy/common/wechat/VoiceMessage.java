package com.itphy.common.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class VoiceMessage extends MessageAbstract{
    private String mediaId;

    @Override
    public String toString() {
        return "<xml>" +
                "<ToUserName><![CDATA["+this.toUserName+"]]></ToUserName>" +
                "<FromUserName><![CDATA["+this.fromUserName+"]]></FromUserName>" +
                "<CreateTime>"+System.currentTimeMillis()/1000+"</CreateTime>" +
                "<MsgType><![CDATA[voice]]></MsgType>" +
                "<Voice>" +
                "<MediaId><![CDATA["+this.mediaId+"]]></MediaId>" +
                "</Voice>" +
                "</xml>";
    }
}
