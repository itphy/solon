package com.itphy.common.wechat;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TextMessage extends MessageAbstract{
    private String content;

    @Override
    public String toString() {
        return "<xml>" +
                "<ToUserName><![CDATA["+this.toUserName+"]]></ToUserName>" +
                "<FromUserName><![CDATA["+this.fromUserName+"]]></FromUserName>" +
                "<CreateTime>"+System.currentTimeMillis()/1000+"</CreateTime>" +
                "<MsgType><![CDATA[text]]></MsgType>" +
                "<Content><![CDATA["+this.content+"]]></Content>" +
                "</xml>";
    }
}
