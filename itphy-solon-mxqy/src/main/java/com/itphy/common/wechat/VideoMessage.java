package com.itphy.common.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class VideoMessage extends MessageAbstract{
    private String mediaId;
    private String title;
    private String description;

    @Override
    public String toString() {
        return "<xml>" +
                "<ToUserName><![CDATA["+this.toUserName+"]]></ToUserName>" +
                "<FromUserName><![CDATA["+this.fromUserName+"]]></FromUserName>" +
                "<CreateTime>"+System.currentTimeMillis()/1000+"</CreateTime>" +
                "<MsgType><![CDATA[video]]></MsgType>" +
                "<Video>" +
                "<MediaId><![CDATA["+this.mediaId+"]]></MediaId>" +
                "<Title><![CDATA["+this.title+"]]></Title>" +
                "<Description><![CDATA["+this.description+"]]></Description>" +
                "</Video>" +
                "</xml>";
    }
}
