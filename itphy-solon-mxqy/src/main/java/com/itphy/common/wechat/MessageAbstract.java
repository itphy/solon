package com.itphy.common.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public abstract class MessageAbstract {
    public String toUserName;
    public String fromUserName;
}
