package com.itphy.common.wechat;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(chain = true)
public class TemplateMessage {
    private String touser;
    private String template_id;
    private String url;
    private MiniProgram miniprogram;
    private Map<String,Object> data;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
