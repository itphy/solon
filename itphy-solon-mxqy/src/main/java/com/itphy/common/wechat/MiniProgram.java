package com.itphy.common.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MiniProgram {
    private String appid;
    private String pagepath;
}
