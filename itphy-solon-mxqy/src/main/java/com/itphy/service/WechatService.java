package com.itphy.service;

import com.itphy.common.wechat.TemplateMessage;

public interface WechatService {
    String getAccessToken();
    boolean sendTemplateMessage(TemplateMessage message);
}
