package com.itphy.service.Impl;

import com.alibaba.fastjson.JSON;
import com.itphy.mapper.UserInfoMapper;
import com.itphy.pojo.UserInfo;
import com.itphy.service.LoginService;
import com.itphy.util.JwtUtils;
import com.itphy.util.MD5;
import com.itphy.util.Maps;
import org.noear.solon.annotation.Inject;
import org.noear.solon.extend.aspect.annotation.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class LoginServiceImpl implements LoginService {
    @Inject
    private UserInfoMapper userInfoMapper;
    @Override
    public String login(UserInfo userInfo) {
        UserInfo params=Maps.newInstance().put("account_no", userInfo.getAccountNo()).parse(UserInfo.class);
        UserInfo loginUser = userInfoMapper.queryUserInfoLimit1(params);
        if(!Objects.isNull(loginUser) && loginUser.getPassword().equals(MD5.check(userInfo.getPassword()))){
            return JwtUtils.getJwt(loginUser);
        }else {
            return null;
        }
    }
}
