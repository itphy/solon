package com.itphy.service;

import com.itphy.pojo.UserInfo;

import java.util.Map;

public interface LoginService {

    String login(UserInfo userInfo);
}
