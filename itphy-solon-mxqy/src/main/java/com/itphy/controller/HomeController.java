package com.itphy.controller;

import com.alibaba.fastjson.JSON;
import com.itphy.common.R;
import com.itphy.intercept.LoginIntercept;
import com.itphy.mapper.UserInfoMapper;
import com.itphy.mapper.base.UserInfoBaseMapper;
import com.itphy.pojo.UserInfo;
import com.itphy.util.RedisClient;
import com.itphy.util.SystemUtil;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.extend.cors.annotation.CrossOrigin;
import redis.clients.jedis.Jedis;

import java.util.Objects;

@Controller
@CrossOrigin
public class HomeController {

    @Mapping(value = "/")
    public ModelAndView home(Context ctx){
        ctx.charset("utf-8");
        return new ModelAndView("index.html");
    }

    @Before(LoginIntercept.class)
    @Mapping(value = "/dialog")
    public ModelAndView dialog(Context ctx){
        return new ModelAndView("dialog.html");
    }

    @Mapping(value = "/login",method = MethodType.GET)
    public ModelAndView login(Context ctx){
        return new ModelAndView("login.html");
    }

    @Mapping(value = "/QQ")
    public ModelAndView qq(){
        return new ModelAndView("index.html");
    }

    @Mapping(value = "/allMsg",method = MethodType.GET)
    public R getMsg(Context ctx){
        Jedis jedis = null;
        String s="";
        try {
            jedis=RedisClient.getJedis();
            s = jedis.get("wechat::001");
            if(Objects.isNull(s)){
                return R.fail();
            }
            return R.ok().data(JSON.parse(s));
        }catch (Exception e){
            return R.fail();
        }finally {
            if(jedis!=null){
                jedis.close();
            }
        }
    }

    @Mapping(value = "/getMxQyMsg",method = MethodType.GET)
    public R getMxQyMsg(Context ctx){
        SystemUtil.isCross(ctx,true);
        Jedis jedis = null;
        String s="";
        try {
            jedis=RedisClient.getJedis();
            s = jedis.get("MXQY::001");
            if(Objects.isNull(s)){
                return R.fail();
            }
            return R.ok().data(JSON.parse(s));
        }catch (Exception e){
            return R.fail();
        }finally {
            if(jedis!=null){
                jedis.close();
            }
        }
    }

    @Inject
    private UserInfoMapper userInfoMapper;
    @Mapping("/user")
    public R getUserInfo(){
        UserInfo userInfo = new UserInfo();
        userInfo.setId(1l);
        return R.ok().data(userInfoMapper.queryUserInfoLimit1(userInfo));
    }
}
