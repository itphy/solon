package com.itphy.controller;

import com.itphy.common.R;
import com.itphy.pojo.UserInfo;
import com.itphy.service.LoginService;
import org.apache.ibatis.annotations.Mapper;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Param;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;

import java.util.Objects;

@Controller
public class LoginController {
    @Inject
    private LoginService loginService;

    @Mapping(value = "/login",method = MethodType.POST)
    public R login(@Param UserInfo userInfo, Context ctx){
       String token = loginService.login(userInfo);
        if(Objects.isNull(token)){
            return R.fail();
        }
        ctx.session("token",token);
        return R.ok().data(token);
    }
}
