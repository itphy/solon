package com.itphy.util;

import org.noear.solon.core.handle.Context;

public class SystemUtil {

    public static boolean isWin(){
        return System.getProperties().getProperty("os.name").startsWith("Windows");
    }

    /**
     * 是否开启跨域
     * @param ctx
     * @param flag true 开启跨域, false 否
     */
    public static void isCross(Context ctx, boolean flag){
        if(flag){
            ctx.headerAdd("Access-Control-Allow-Origin","*");
        } else {
            ctx.headerAdd("Access-Control-Allow-Origin","http://love.itphy.com:9999");
        }
    }
}
