package com.itphy.util;

import com.itphy.pojo.UserInfo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class JwtUtils {
    public static final String SUBJECT = "RookieLi";
    public static final String SECRET_KEY = "VKPybddNje5hdKJc"; // 密钥

    public static final long EXPIRE = 1000 * 60 * 60 * 3;  //过期时间，毫秒，3个小时

    public static String getJwt(UserInfo user) {
        return Jwts.builder()
                .setSubject(SUBJECT)
                .claim("id", user.getId())
                .claim("name", user.getUserName())
                .claim("accountNo",user.getAccountNo())
                .setIssuedAt(new Date())
                .setIssuer("itphy")
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRE))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    public static Claims checkJWT(String token) {
        try {
            final Claims claims = Jwts.parser()
                    .setSigningKey(SECRET_KEY)
                    .parseClaimsJws(token)
                    .getBody();
            return claims;
        } catch (Exception e) {
            return null;
        }
    }
}
