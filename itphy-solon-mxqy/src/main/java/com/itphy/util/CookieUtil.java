package com.itphy.util;

import org.noear.solon.core.message.Session;

import java.util.Map;

public class CookieUtil {

    public static Map<String,Object> getCookieMap(Session session){
        String[] kvs = session.header("cookie").split("\\;");
        Maps maps = Maps.newInstance();
        for (String k_v : kvs) {
            try {
                String[] kv = k_v.split("\\=");
                maps.put(kv[0].trim(),kv[1].trim());
            }catch (Exception e){

            }
        }
        return maps.getData();
    }
}
