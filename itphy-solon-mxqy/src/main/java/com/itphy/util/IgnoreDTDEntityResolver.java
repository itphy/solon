package com.itphy.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * 
 * @author p4776
 */
public class IgnoreDTDEntityResolver implements EntityResolver {
	

	/***
	 * resolve Entity
	 * @param publicId
	 * @param systemId
	 */
	@Override
    public InputSource resolveEntity(String publicId, String systemId)
			throws SAXException, IOException {
		return new InputSource(new ByteArrayInputStream(
				"<?xml version='1.0' encoding='UTF-8'?>".getBytes()));
	}
}
