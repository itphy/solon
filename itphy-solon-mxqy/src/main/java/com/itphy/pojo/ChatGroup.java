package com.itphy.pojo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class ChatGroup implements Serializable {

    private static final long serialVersionUID = 1621761052656L;


    /**
    * 主键
    * 
    * isNullAble:0
    */
    private Long id;

    /**
    * 群聊名称
    * isNullAble:0
    */
    private String chatName;

    /**
    * 群聊号
    * isNullAble:0
    */
    private String chatNo;


    public void setId(Long id){this.id = id;}

    public Long getId(){return this.id;}

    public void setChatName(String chatName){this.chatName = chatName;}

    public String getChatName(){return this.chatName;}

    public void setChatNo(String chatNo){this.chatNo = chatNo;}

    public String getChatNo(){return this.chatNo;}
    @Override
    public String toString() {
        return "ChatGroup{" +
                "id='" + id + '\'' +
                "chatName='" + chatName + '\'' +
                "chatNo='" + chatNo + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private ChatGroup set;

        private ConditionBuilder where;

        public UpdateBuilder set(ChatGroup set){
            this.set = set;
            return this;
        }

        public ChatGroup getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends ChatGroup{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<Long> idList;

        public List<Long> getIdList(){return this.idList;}

        private Long idSt;

        private Long idEd;

        public Long getIdSt(){return this.idSt;}

        public Long getIdEd(){return this.idEd;}

        private List<String> chatNameList;

        public List<String> getChatNameList(){return this.chatNameList;}


        private List<String> fuzzyChatName;

        public List<String> getFuzzyChatName(){return this.fuzzyChatName;}

        private List<String> rightFuzzyChatName;

        public List<String> getRightFuzzyChatName(){return this.rightFuzzyChatName;}
        private List<String> chatNoList;

        public List<String> getChatNoList(){return this.chatNoList;}


        private List<String> fuzzyChatNo;

        public List<String> getFuzzyChatNo(){return this.fuzzyChatNo;}

        private List<String> rightFuzzyChatNo;

        public List<String> getRightFuzzyChatNo(){return this.rightFuzzyChatNo;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder idBetWeen(Long idSt,Long idEd){
            this.idSt = idSt;
            this.idEd = idEd;
            return this;
        }

        public QueryBuilder idGreaterEqThan(Long idSt){
            this.idSt = idSt;
            return this;
        }
        public QueryBuilder idLessEqThan(Long idEd){
            this.idEd = idEd;
            return this;
        }


        public QueryBuilder id(Long id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(Long ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<Long> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzyChatName (List<String> fuzzyChatName){
            this.fuzzyChatName = fuzzyChatName;
            return this;
        }

        public QueryBuilder fuzzyChatName (String ... fuzzyChatName){
            this.fuzzyChatName = solveNullList(fuzzyChatName);
            return this;
        }

        public QueryBuilder rightFuzzyChatName (List<String> rightFuzzyChatName){
            this.rightFuzzyChatName = rightFuzzyChatName;
            return this;
        }

        public QueryBuilder rightFuzzyChatName (String ... rightFuzzyChatName){
            this.rightFuzzyChatName = solveNullList(rightFuzzyChatName);
            return this;
        }

        public QueryBuilder chatName(String chatName){
            setChatName(chatName);
            return this;
        }

        public QueryBuilder chatNameList(String ... chatName){
            this.chatNameList = solveNullList(chatName);
            return this;
        }

        public QueryBuilder chatNameList(List<String> chatName){
            this.chatNameList = chatName;
            return this;
        }

        public QueryBuilder fetchChatName(){
            setFetchFields("fetchFields","chatName");
            return this;
        }

        public QueryBuilder excludeChatName(){
            setFetchFields("excludeFields","chatName");
            return this;
        }

        public QueryBuilder fuzzyChatNo (List<String> fuzzyChatNo){
            this.fuzzyChatNo = fuzzyChatNo;
            return this;
        }

        public QueryBuilder fuzzyChatNo (String ... fuzzyChatNo){
            this.fuzzyChatNo = solveNullList(fuzzyChatNo);
            return this;
        }

        public QueryBuilder rightFuzzyChatNo (List<String> rightFuzzyChatNo){
            this.rightFuzzyChatNo = rightFuzzyChatNo;
            return this;
        }

        public QueryBuilder rightFuzzyChatNo (String ... rightFuzzyChatNo){
            this.rightFuzzyChatNo = solveNullList(rightFuzzyChatNo);
            return this;
        }

        public QueryBuilder chatNo(String chatNo){
            setChatNo(chatNo);
            return this;
        }

        public QueryBuilder chatNoList(String ... chatNo){
            this.chatNoList = solveNullList(chatNo);
            return this;
        }

        public QueryBuilder chatNoList(List<String> chatNo){
            this.chatNoList = chatNo;
            return this;
        }

        public QueryBuilder fetchChatNo(){
            setFetchFields("fetchFields","chatNo");
            return this;
        }

        public QueryBuilder excludeChatNo(){
            setFetchFields("excludeFields","chatNo");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public ChatGroup build(){return this;}
    }


    public static class ConditionBuilder{
        private List<Long> idList;

        public List<Long> getIdList(){return this.idList;}

        private Long idSt;

        private Long idEd;

        public Long getIdSt(){return this.idSt;}

        public Long getIdEd(){return this.idEd;}

        private List<String> chatNameList;

        public List<String> getChatNameList(){return this.chatNameList;}


        private List<String> fuzzyChatName;

        public List<String> getFuzzyChatName(){return this.fuzzyChatName;}

        private List<String> rightFuzzyChatName;

        public List<String> getRightFuzzyChatName(){return this.rightFuzzyChatName;}
        private List<String> chatNoList;

        public List<String> getChatNoList(){return this.chatNoList;}


        private List<String> fuzzyChatNo;

        public List<String> getFuzzyChatNo(){return this.fuzzyChatNo;}

        private List<String> rightFuzzyChatNo;

        public List<String> getRightFuzzyChatNo(){return this.rightFuzzyChatNo;}

        public ConditionBuilder idBetWeen(Long idSt,Long idEd){
            this.idSt = idSt;
            this.idEd = idEd;
            return this;
        }

        public ConditionBuilder idGreaterEqThan(Long idSt){
            this.idSt = idSt;
            return this;
        }
        public ConditionBuilder idLessEqThan(Long idEd){
            this.idEd = idEd;
            return this;
        }


        public ConditionBuilder idList(Long ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<Long> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzyChatName (List<String> fuzzyChatName){
            this.fuzzyChatName = fuzzyChatName;
            return this;
        }

        public ConditionBuilder fuzzyChatName (String ... fuzzyChatName){
            this.fuzzyChatName = solveNullList(fuzzyChatName);
            return this;
        }

        public ConditionBuilder rightFuzzyChatName (List<String> rightFuzzyChatName){
            this.rightFuzzyChatName = rightFuzzyChatName;
            return this;
        }

        public ConditionBuilder rightFuzzyChatName (String ... rightFuzzyChatName){
            this.rightFuzzyChatName = solveNullList(rightFuzzyChatName);
            return this;
        }

        public ConditionBuilder chatNameList(String ... chatName){
            this.chatNameList = solveNullList(chatName);
            return this;
        }

        public ConditionBuilder chatNameList(List<String> chatName){
            this.chatNameList = chatName;
            return this;
        }

        public ConditionBuilder fuzzyChatNo (List<String> fuzzyChatNo){
            this.fuzzyChatNo = fuzzyChatNo;
            return this;
        }

        public ConditionBuilder fuzzyChatNo (String ... fuzzyChatNo){
            this.fuzzyChatNo = solveNullList(fuzzyChatNo);
            return this;
        }

        public ConditionBuilder rightFuzzyChatNo (List<String> rightFuzzyChatNo){
            this.rightFuzzyChatNo = rightFuzzyChatNo;
            return this;
        }

        public ConditionBuilder rightFuzzyChatNo (String ... rightFuzzyChatNo){
            this.rightFuzzyChatNo = solveNullList(rightFuzzyChatNo);
            return this;
        }

        public ConditionBuilder chatNoList(String ... chatNo){
            this.chatNoList = solveNullList(chatNo);
            return this;
        }

        public ConditionBuilder chatNoList(List<String> chatNo){
            this.chatNoList = chatNo;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private ChatGroup obj;

        public Builder(){
            this.obj = new ChatGroup();
        }

        public Builder id(Long id){
            this.obj.setId(id);
            return this;
        }
        public Builder chatName(String chatName){
            this.obj.setChatName(chatName);
            return this;
        }
        public Builder chatNo(String chatNo){
            this.obj.setChatNo(chatNo);
            return this;
        }
        public ChatGroup build(){return obj;}
    }

}
