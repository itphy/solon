package com.itphy.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class WechatTemplate {
    private Long id;
    private String template;
    private String key;
}
