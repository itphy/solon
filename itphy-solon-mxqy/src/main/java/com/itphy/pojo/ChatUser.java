package com.itphy.pojo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class ChatUser implements Serializable {

    private static final long serialVersionUID = 1621761093203L;


    /**
    * 主键
    * 
    * isNullAble:0
    */
    private Long id;

    /**
    * 
    * isNullAble:0
    */
    private Long chatId;

    /**
    * 
    * isNullAble:0
    */
    private Long userId;


    public void setId(Long id){this.id = id;}

    public Long getId(){return this.id;}

    public void setChatId(Long chatId){this.chatId = chatId;}

    public Long getChatId(){return this.chatId;}

    public void setUserId(Long userId){this.userId = userId;}

    public Long getUserId(){return this.userId;}
    @Override
    public String toString() {
        return "ChatUser{" +
                "id='" + id + '\'' +
                "chatId='" + chatId + '\'' +
                "userId='" + userId + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private ChatUser set;

        private ConditionBuilder where;

        public UpdateBuilder set(ChatUser set){
            this.set = set;
            return this;
        }

        public ChatUser getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends ChatUser{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<Long> idList;

        public List<Long> getIdList(){return this.idList;}

        private Long idSt;

        private Long idEd;

        public Long getIdSt(){return this.idSt;}

        public Long getIdEd(){return this.idEd;}

        private List<Long> chatIdList;

        public List<Long> getChatIdList(){return this.chatIdList;}

        private Long chatIdSt;

        private Long chatIdEd;

        public Long getChatIdSt(){return this.chatIdSt;}

        public Long getChatIdEd(){return this.chatIdEd;}

        private List<Long> userIdList;

        public List<Long> getUserIdList(){return this.userIdList;}

        private Long userIdSt;

        private Long userIdEd;

        public Long getUserIdSt(){return this.userIdSt;}

        public Long getUserIdEd(){return this.userIdEd;}

        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder idBetWeen(Long idSt,Long idEd){
            this.idSt = idSt;
            this.idEd = idEd;
            return this;
        }

        public QueryBuilder idGreaterEqThan(Long idSt){
            this.idSt = idSt;
            return this;
        }
        public QueryBuilder idLessEqThan(Long idEd){
            this.idEd = idEd;
            return this;
        }


        public QueryBuilder id(Long id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(Long ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<Long> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder chatIdBetWeen(Long chatIdSt,Long chatIdEd){
            this.chatIdSt = chatIdSt;
            this.chatIdEd = chatIdEd;
            return this;
        }

        public QueryBuilder chatIdGreaterEqThan(Long chatIdSt){
            this.chatIdSt = chatIdSt;
            return this;
        }
        public QueryBuilder chatIdLessEqThan(Long chatIdEd){
            this.chatIdEd = chatIdEd;
            return this;
        }


        public QueryBuilder chatId(Long chatId){
            setChatId(chatId);
            return this;
        }

        public QueryBuilder chatIdList(Long ... chatId){
            this.chatIdList = solveNullList(chatId);
            return this;
        }

        public QueryBuilder chatIdList(List<Long> chatId){
            this.chatIdList = chatId;
            return this;
        }

        public QueryBuilder fetchChatId(){
            setFetchFields("fetchFields","chatId");
            return this;
        }

        public QueryBuilder excludeChatId(){
            setFetchFields("excludeFields","chatId");
            return this;
        }

        public QueryBuilder userIdBetWeen(Long userIdSt,Long userIdEd){
            this.userIdSt = userIdSt;
            this.userIdEd = userIdEd;
            return this;
        }

        public QueryBuilder userIdGreaterEqThan(Long userIdSt){
            this.userIdSt = userIdSt;
            return this;
        }
        public QueryBuilder userIdLessEqThan(Long userIdEd){
            this.userIdEd = userIdEd;
            return this;
        }


        public QueryBuilder userId(Long userId){
            setUserId(userId);
            return this;
        }

        public QueryBuilder userIdList(Long ... userId){
            this.userIdList = solveNullList(userId);
            return this;
        }

        public QueryBuilder userIdList(List<Long> userId){
            this.userIdList = userId;
            return this;
        }

        public QueryBuilder fetchUserId(){
            setFetchFields("fetchFields","userId");
            return this;
        }

        public QueryBuilder excludeUserId(){
            setFetchFields("excludeFields","userId");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public ChatUser build(){return this;}
    }


    public static class ConditionBuilder{
        private List<Long> idList;

        public List<Long> getIdList(){return this.idList;}

        private Long idSt;

        private Long idEd;

        public Long getIdSt(){return this.idSt;}

        public Long getIdEd(){return this.idEd;}

        private List<Long> chatIdList;

        public List<Long> getChatIdList(){return this.chatIdList;}

        private Long chatIdSt;

        private Long chatIdEd;

        public Long getChatIdSt(){return this.chatIdSt;}

        public Long getChatIdEd(){return this.chatIdEd;}

        private List<Long> userIdList;

        public List<Long> getUserIdList(){return this.userIdList;}

        private Long userIdSt;

        private Long userIdEd;

        public Long getUserIdSt(){return this.userIdSt;}

        public Long getUserIdEd(){return this.userIdEd;}


        public ConditionBuilder idBetWeen(Long idSt,Long idEd){
            this.idSt = idSt;
            this.idEd = idEd;
            return this;
        }

        public ConditionBuilder idGreaterEqThan(Long idSt){
            this.idSt = idSt;
            return this;
        }
        public ConditionBuilder idLessEqThan(Long idEd){
            this.idEd = idEd;
            return this;
        }


        public ConditionBuilder idList(Long ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<Long> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder chatIdBetWeen(Long chatIdSt,Long chatIdEd){
            this.chatIdSt = chatIdSt;
            this.chatIdEd = chatIdEd;
            return this;
        }

        public ConditionBuilder chatIdGreaterEqThan(Long chatIdSt){
            this.chatIdSt = chatIdSt;
            return this;
        }
        public ConditionBuilder chatIdLessEqThan(Long chatIdEd){
            this.chatIdEd = chatIdEd;
            return this;
        }


        public ConditionBuilder chatIdList(Long ... chatId){
            this.chatIdList = solveNullList(chatId);
            return this;
        }

        public ConditionBuilder chatIdList(List<Long> chatId){
            this.chatIdList = chatId;
            return this;
        }

        public ConditionBuilder userIdBetWeen(Long userIdSt,Long userIdEd){
            this.userIdSt = userIdSt;
            this.userIdEd = userIdEd;
            return this;
        }

        public ConditionBuilder userIdGreaterEqThan(Long userIdSt){
            this.userIdSt = userIdSt;
            return this;
        }
        public ConditionBuilder userIdLessEqThan(Long userIdEd){
            this.userIdEd = userIdEd;
            return this;
        }


        public ConditionBuilder userIdList(Long ... userId){
            this.userIdList = solveNullList(userId);
            return this;
        }

        public ConditionBuilder userIdList(List<Long> userId){
            this.userIdList = userId;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private ChatUser obj;

        public Builder(){
            this.obj = new ChatUser();
        }

        public Builder id(Long id){
            this.obj.setId(id);
            return this;
        }
        public Builder chatId(Long chatId){
            this.obj.setChatId(chatId);
            return this;
        }
        public Builder userId(Long userId){
            this.obj.setUserId(userId);
            return this;
        }
        public ChatUser build(){return obj;}
    }

}
