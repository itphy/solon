package com.itphy.pojo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1621761097962L;


    /**
    * 主键
    * 
    * isNullAble:0
    */
    private Long id;

    /**
    * 用户账号
    * isNullAble:0
    */
    private String accountNo;

    /**
    * 用户密码
    * isNullAble:0
    */
    private String password;

    /**
    * 创建时间
    * isNullAble:1
    */
    private Long createTime;

    /**
    * 更新时间
    * isNullAble:1
    */
    private Long updateTime;

    /**
    * 头像url
    * isNullAble:1
    */
    private String headerUrl;

    /**
    * 昵称
    * isNullAble:1
    */
    private String userName;


    public void setId(Long id){this.id = id;}

    public Long getId(){return this.id;}

    public void setAccountNo(String accountNo){this.accountNo = accountNo;}

    public String getAccountNo(){return this.accountNo;}

    public void setPassword(String password){this.password = password;}

    public String getPassword(){return this.password;}

    public void setCreateTime(Long createTime){this.createTime = createTime;}

    public Long getCreateTime(){return this.createTime;}

    public void setUpdateTime(Long updateTime){this.updateTime = updateTime;}

    public Long getUpdateTime(){return this.updateTime;}

    public void setHeaderUrl(String headerUrl){this.headerUrl = headerUrl;}

    public String getHeaderUrl(){return this.headerUrl;}

    public void setUserName(String userName){this.userName = userName;}

    public String getUserName(){return this.userName;}
    @Override
    public String toString() {
        return "UserInfo{" +
                "id='" + id + '\'' +
                "accountNo='" + accountNo + '\'' +
                "password='" + password + '\'' +
                "createTime='" + createTime + '\'' +
                "updateTime='" + updateTime + '\'' +
                "headerUrl='" + headerUrl + '\'' +
                "userName='" + userName + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private UserInfo set;

        private ConditionBuilder where;

        public UpdateBuilder set(UserInfo set){
            this.set = set;
            return this;
        }

        public UserInfo getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends UserInfo{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<Long> idList;

        public List<Long> getIdList(){return this.idList;}

        private Long idSt;

        private Long idEd;

        public Long getIdSt(){return this.idSt;}

        public Long getIdEd(){return this.idEd;}

        private List<String> accountNoList;

        public List<String> getAccountNoList(){return this.accountNoList;}


        private List<String> fuzzyAccountNo;

        public List<String> getFuzzyAccountNo(){return this.fuzzyAccountNo;}

        private List<String> rightFuzzyAccountNo;

        public List<String> getRightFuzzyAccountNo(){return this.rightFuzzyAccountNo;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<Long> createTimeList;

        public List<Long> getCreateTimeList(){return this.createTimeList;}

        private Long createTimeSt;

        private Long createTimeEd;

        public Long getCreateTimeSt(){return this.createTimeSt;}

        public Long getCreateTimeEd(){return this.createTimeEd;}

        private List<Long> updateTimeList;

        public List<Long> getUpdateTimeList(){return this.updateTimeList;}

        private Long updateTimeSt;

        private Long updateTimeEd;

        public Long getUpdateTimeSt(){return this.updateTimeSt;}

        public Long getUpdateTimeEd(){return this.updateTimeEd;}

        private List<String> headerUrlList;

        public List<String> getHeaderUrlList(){return this.headerUrlList;}


        private List<String> fuzzyHeaderUrl;

        public List<String> getFuzzyHeaderUrl(){return this.fuzzyHeaderUrl;}

        private List<String> rightFuzzyHeaderUrl;

        public List<String> getRightFuzzyHeaderUrl(){return this.rightFuzzyHeaderUrl;}
        private List<String> userNameList;

        public List<String> getUserNameList(){return this.userNameList;}


        private List<String> fuzzyUserName;

        public List<String> getFuzzyUserName(){return this.fuzzyUserName;}

        private List<String> rightFuzzyUserName;

        public List<String> getRightFuzzyUserName(){return this.rightFuzzyUserName;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder idBetWeen(Long idSt,Long idEd){
            this.idSt = idSt;
            this.idEd = idEd;
            return this;
        }

        public QueryBuilder idGreaterEqThan(Long idSt){
            this.idSt = idSt;
            return this;
        }
        public QueryBuilder idLessEqThan(Long idEd){
            this.idEd = idEd;
            return this;
        }


        public QueryBuilder id(Long id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(Long ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<Long> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzyAccountNo (List<String> fuzzyAccountNo){
            this.fuzzyAccountNo = fuzzyAccountNo;
            return this;
        }

        public QueryBuilder fuzzyAccountNo (String ... fuzzyAccountNo){
            this.fuzzyAccountNo = solveNullList(fuzzyAccountNo);
            return this;
        }

        public QueryBuilder rightFuzzyAccountNo (List<String> rightFuzzyAccountNo){
            this.rightFuzzyAccountNo = rightFuzzyAccountNo;
            return this;
        }

        public QueryBuilder rightFuzzyAccountNo (String ... rightFuzzyAccountNo){
            this.rightFuzzyAccountNo = solveNullList(rightFuzzyAccountNo);
            return this;
        }

        public QueryBuilder accountNo(String accountNo){
            setAccountNo(accountNo);
            return this;
        }

        public QueryBuilder accountNoList(String ... accountNo){
            this.accountNoList = solveNullList(accountNo);
            return this;
        }

        public QueryBuilder accountNoList(List<String> accountNo){
            this.accountNoList = accountNo;
            return this;
        }

        public QueryBuilder fetchAccountNo(){
            setFetchFields("fetchFields","accountNo");
            return this;
        }

        public QueryBuilder excludeAccountNo(){
            setFetchFields("excludeFields","accountNo");
            return this;
        }

        public QueryBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public QueryBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public QueryBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public QueryBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public QueryBuilder password(String password){
            setPassword(password);
            return this;
        }

        public QueryBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public QueryBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public QueryBuilder fetchPassword(){
            setFetchFields("fetchFields","password");
            return this;
        }

        public QueryBuilder excludePassword(){
            setFetchFields("excludeFields","password");
            return this;
        }

        public QueryBuilder createTimeBetWeen(Long createTimeSt,Long createTimeEd){
            this.createTimeSt = createTimeSt;
            this.createTimeEd = createTimeEd;
            return this;
        }

        public QueryBuilder createTimeGreaterEqThan(Long createTimeSt){
            this.createTimeSt = createTimeSt;
            return this;
        }
        public QueryBuilder createTimeLessEqThan(Long createTimeEd){
            this.createTimeEd = createTimeEd;
            return this;
        }


        public QueryBuilder createTime(Long createTime){
            setCreateTime(createTime);
            return this;
        }

        public QueryBuilder createTimeList(Long ... createTime){
            this.createTimeList = solveNullList(createTime);
            return this;
        }

        public QueryBuilder createTimeList(List<Long> createTime){
            this.createTimeList = createTime;
            return this;
        }

        public QueryBuilder fetchCreateTime(){
            setFetchFields("fetchFields","createTime");
            return this;
        }

        public QueryBuilder excludeCreateTime(){
            setFetchFields("excludeFields","createTime");
            return this;
        }

        public QueryBuilder updateTimeBetWeen(Long updateTimeSt,Long updateTimeEd){
            this.updateTimeSt = updateTimeSt;
            this.updateTimeEd = updateTimeEd;
            return this;
        }

        public QueryBuilder updateTimeGreaterEqThan(Long updateTimeSt){
            this.updateTimeSt = updateTimeSt;
            return this;
        }
        public QueryBuilder updateTimeLessEqThan(Long updateTimeEd){
            this.updateTimeEd = updateTimeEd;
            return this;
        }


        public QueryBuilder updateTime(Long updateTime){
            setUpdateTime(updateTime);
            return this;
        }

        public QueryBuilder updateTimeList(Long ... updateTime){
            this.updateTimeList = solveNullList(updateTime);
            return this;
        }

        public QueryBuilder updateTimeList(List<Long> updateTime){
            this.updateTimeList = updateTime;
            return this;
        }

        public QueryBuilder fetchUpdateTime(){
            setFetchFields("fetchFields","updateTime");
            return this;
        }

        public QueryBuilder excludeUpdateTime(){
            setFetchFields("excludeFields","updateTime");
            return this;
        }

        public QueryBuilder fuzzyHeaderUrl (List<String> fuzzyHeaderUrl){
            this.fuzzyHeaderUrl = fuzzyHeaderUrl;
            return this;
        }

        public QueryBuilder fuzzyHeaderUrl (String ... fuzzyHeaderUrl){
            this.fuzzyHeaderUrl = solveNullList(fuzzyHeaderUrl);
            return this;
        }

        public QueryBuilder rightFuzzyHeaderUrl (List<String> rightFuzzyHeaderUrl){
            this.rightFuzzyHeaderUrl = rightFuzzyHeaderUrl;
            return this;
        }

        public QueryBuilder rightFuzzyHeaderUrl (String ... rightFuzzyHeaderUrl){
            this.rightFuzzyHeaderUrl = solveNullList(rightFuzzyHeaderUrl);
            return this;
        }

        public QueryBuilder headerUrl(String headerUrl){
            setHeaderUrl(headerUrl);
            return this;
        }

        public QueryBuilder headerUrlList(String ... headerUrl){
            this.headerUrlList = solveNullList(headerUrl);
            return this;
        }

        public QueryBuilder headerUrlList(List<String> headerUrl){
            this.headerUrlList = headerUrl;
            return this;
        }

        public QueryBuilder fetchHeaderUrl(){
            setFetchFields("fetchFields","headerUrl");
            return this;
        }

        public QueryBuilder excludeHeaderUrl(){
            setFetchFields("excludeFields","headerUrl");
            return this;
        }

        public QueryBuilder fuzzyUserName (List<String> fuzzyUserName){
            this.fuzzyUserName = fuzzyUserName;
            return this;
        }

        public QueryBuilder fuzzyUserName (String ... fuzzyUserName){
            this.fuzzyUserName = solveNullList(fuzzyUserName);
            return this;
        }

        public QueryBuilder rightFuzzyUserName (List<String> rightFuzzyUserName){
            this.rightFuzzyUserName = rightFuzzyUserName;
            return this;
        }

        public QueryBuilder rightFuzzyUserName (String ... rightFuzzyUserName){
            this.rightFuzzyUserName = solveNullList(rightFuzzyUserName);
            return this;
        }

        public QueryBuilder userName(String userName){
            setUserName(userName);
            return this;
        }

        public QueryBuilder userNameList(String ... userName){
            this.userNameList = solveNullList(userName);
            return this;
        }

        public QueryBuilder userNameList(List<String> userName){
            this.userNameList = userName;
            return this;
        }

        public QueryBuilder fetchUserName(){
            setFetchFields("fetchFields","userName");
            return this;
        }

        public QueryBuilder excludeUserName(){
            setFetchFields("excludeFields","userName");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public UserInfo build(){return this;}
    }


    public static class ConditionBuilder{
        private List<Long> idList;

        public List<Long> getIdList(){return this.idList;}

        private Long idSt;

        private Long idEd;

        public Long getIdSt(){return this.idSt;}

        public Long getIdEd(){return this.idEd;}

        private List<String> accountNoList;

        public List<String> getAccountNoList(){return this.accountNoList;}


        private List<String> fuzzyAccountNo;

        public List<String> getFuzzyAccountNo(){return this.fuzzyAccountNo;}

        private List<String> rightFuzzyAccountNo;

        public List<String> getRightFuzzyAccountNo(){return this.rightFuzzyAccountNo;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<Long> createTimeList;

        public List<Long> getCreateTimeList(){return this.createTimeList;}

        private Long createTimeSt;

        private Long createTimeEd;

        public Long getCreateTimeSt(){return this.createTimeSt;}

        public Long getCreateTimeEd(){return this.createTimeEd;}

        private List<Long> updateTimeList;

        public List<Long> getUpdateTimeList(){return this.updateTimeList;}

        private Long updateTimeSt;

        private Long updateTimeEd;

        public Long getUpdateTimeSt(){return this.updateTimeSt;}

        public Long getUpdateTimeEd(){return this.updateTimeEd;}

        private List<String> headerUrlList;

        public List<String> getHeaderUrlList(){return this.headerUrlList;}


        private List<String> fuzzyHeaderUrl;

        public List<String> getFuzzyHeaderUrl(){return this.fuzzyHeaderUrl;}

        private List<String> rightFuzzyHeaderUrl;

        public List<String> getRightFuzzyHeaderUrl(){return this.rightFuzzyHeaderUrl;}
        private List<String> userNameList;

        public List<String> getUserNameList(){return this.userNameList;}


        private List<String> fuzzyUserName;

        public List<String> getFuzzyUserName(){return this.fuzzyUserName;}

        private List<String> rightFuzzyUserName;

        public List<String> getRightFuzzyUserName(){return this.rightFuzzyUserName;}

        public ConditionBuilder idBetWeen(Long idSt,Long idEd){
            this.idSt = idSt;
            this.idEd = idEd;
            return this;
        }

        public ConditionBuilder idGreaterEqThan(Long idSt){
            this.idSt = idSt;
            return this;
        }
        public ConditionBuilder idLessEqThan(Long idEd){
            this.idEd = idEd;
            return this;
        }


        public ConditionBuilder idList(Long ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<Long> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzyAccountNo (List<String> fuzzyAccountNo){
            this.fuzzyAccountNo = fuzzyAccountNo;
            return this;
        }

        public ConditionBuilder fuzzyAccountNo (String ... fuzzyAccountNo){
            this.fuzzyAccountNo = solveNullList(fuzzyAccountNo);
            return this;
        }

        public ConditionBuilder rightFuzzyAccountNo (List<String> rightFuzzyAccountNo){
            this.rightFuzzyAccountNo = rightFuzzyAccountNo;
            return this;
        }

        public ConditionBuilder rightFuzzyAccountNo (String ... rightFuzzyAccountNo){
            this.rightFuzzyAccountNo = solveNullList(rightFuzzyAccountNo);
            return this;
        }

        public ConditionBuilder accountNoList(String ... accountNo){
            this.accountNoList = solveNullList(accountNo);
            return this;
        }

        public ConditionBuilder accountNoList(List<String> accountNo){
            this.accountNoList = accountNo;
            return this;
        }

        public ConditionBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public ConditionBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public ConditionBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public ConditionBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public ConditionBuilder createTimeBetWeen(Long createTimeSt,Long createTimeEd){
            this.createTimeSt = createTimeSt;
            this.createTimeEd = createTimeEd;
            return this;
        }

        public ConditionBuilder createTimeGreaterEqThan(Long createTimeSt){
            this.createTimeSt = createTimeSt;
            return this;
        }
        public ConditionBuilder createTimeLessEqThan(Long createTimeEd){
            this.createTimeEd = createTimeEd;
            return this;
        }


        public ConditionBuilder createTimeList(Long ... createTime){
            this.createTimeList = solveNullList(createTime);
            return this;
        }

        public ConditionBuilder createTimeList(List<Long> createTime){
            this.createTimeList = createTime;
            return this;
        }

        public ConditionBuilder updateTimeBetWeen(Long updateTimeSt,Long updateTimeEd){
            this.updateTimeSt = updateTimeSt;
            this.updateTimeEd = updateTimeEd;
            return this;
        }

        public ConditionBuilder updateTimeGreaterEqThan(Long updateTimeSt){
            this.updateTimeSt = updateTimeSt;
            return this;
        }
        public ConditionBuilder updateTimeLessEqThan(Long updateTimeEd){
            this.updateTimeEd = updateTimeEd;
            return this;
        }


        public ConditionBuilder updateTimeList(Long ... updateTime){
            this.updateTimeList = solveNullList(updateTime);
            return this;
        }

        public ConditionBuilder updateTimeList(List<Long> updateTime){
            this.updateTimeList = updateTime;
            return this;
        }

        public ConditionBuilder fuzzyHeaderUrl (List<String> fuzzyHeaderUrl){
            this.fuzzyHeaderUrl = fuzzyHeaderUrl;
            return this;
        }

        public ConditionBuilder fuzzyHeaderUrl (String ... fuzzyHeaderUrl){
            this.fuzzyHeaderUrl = solveNullList(fuzzyHeaderUrl);
            return this;
        }

        public ConditionBuilder rightFuzzyHeaderUrl (List<String> rightFuzzyHeaderUrl){
            this.rightFuzzyHeaderUrl = rightFuzzyHeaderUrl;
            return this;
        }

        public ConditionBuilder rightFuzzyHeaderUrl (String ... rightFuzzyHeaderUrl){
            this.rightFuzzyHeaderUrl = solveNullList(rightFuzzyHeaderUrl);
            return this;
        }

        public ConditionBuilder headerUrlList(String ... headerUrl){
            this.headerUrlList = solveNullList(headerUrl);
            return this;
        }

        public ConditionBuilder headerUrlList(List<String> headerUrl){
            this.headerUrlList = headerUrl;
            return this;
        }

        public ConditionBuilder fuzzyUserName (List<String> fuzzyUserName){
            this.fuzzyUserName = fuzzyUserName;
            return this;
        }

        public ConditionBuilder fuzzyUserName (String ... fuzzyUserName){
            this.fuzzyUserName = solveNullList(fuzzyUserName);
            return this;
        }

        public ConditionBuilder rightFuzzyUserName (List<String> rightFuzzyUserName){
            this.rightFuzzyUserName = rightFuzzyUserName;
            return this;
        }

        public ConditionBuilder rightFuzzyUserName (String ... rightFuzzyUserName){
            this.rightFuzzyUserName = solveNullList(rightFuzzyUserName);
            return this;
        }

        public ConditionBuilder userNameList(String ... userName){
            this.userNameList = solveNullList(userName);
            return this;
        }

        public ConditionBuilder userNameList(List<String> userName){
            this.userNameList = userName;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private UserInfo obj;

        public Builder(){
            this.obj = new UserInfo();
        }

        public Builder id(Long id){
            this.obj.setId(id);
            return this;
        }
        public Builder accountNo(String accountNo){
            this.obj.setAccountNo(accountNo);
            return this;
        }
        public Builder password(String password){
            this.obj.setPassword(password);
            return this;
        }
        public Builder createTime(Long createTime){
            this.obj.setCreateTime(createTime);
            return this;
        }
        public Builder updateTime(Long updateTime){
            this.obj.setUpdateTime(updateTime);
            return this;
        }
        public Builder headerUrl(String headerUrl){
            this.obj.setHeaderUrl(headerUrl);
            return this;
        }
        public Builder userName(String userName){
            this.obj.setUserName(userName);
            return this;
        }
        public UserInfo build(){return obj;}
    }

}
