package com.itphy.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class WechatSendLog {
    private Long id;
    private Long bussId;
    private String openId;
    private String tempKey;
    private Integer status;
    private String content;
}
