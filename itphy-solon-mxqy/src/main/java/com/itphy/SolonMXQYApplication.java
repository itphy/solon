package com.itphy;

import com.itphy.ws.WeatherService;
import com.itphy.ws.WeatherServiceService;
import org.noear.solon.Solon;

public class SolonMXQYApplication {
    public static void main(String[] args) {
//        Solon.start(SolonMXQYApplication.class, args,app->{
//            app.enableWebSocket(true);//启用websocket
//        });
        WeatherServiceService factory = new WeatherServiceService();
        WeatherService servicePort = factory.getWeatherServicePort();
        String weather = servicePort.getWeatherByCityname("深圳");
        System.out.println(weather);
    }
}
