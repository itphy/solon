package com.itphy.mapper;

import com.itphy.pojo.WechatSendLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface WechatSendLogMapper {
    List<WechatSendLog> queryBeanBySendFail();
    Integer insert(WechatSendLog wechatSendLog);
}
