package com.itphy.mapper.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.itphy.pojo.ChatGroup;
/**
*  @author author
*/
public interface ChatGroupBaseMapper {

    int insertChatGroup(ChatGroup object);

    int updateChatGroup(ChatGroup object);

    int update(ChatGroup.UpdateBuilder object);

    List<ChatGroup> queryChatGroup(ChatGroup object);

    ChatGroup queryChatGroupLimit1(ChatGroup object);

}