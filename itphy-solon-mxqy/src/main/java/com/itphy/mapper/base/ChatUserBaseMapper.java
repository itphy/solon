package com.itphy.mapper.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.itphy.pojo.ChatUser;
/**
*  @author author
*/
public interface ChatUserBaseMapper {

    int insertChatUser(ChatUser object);

    int updateChatUser(ChatUser object);

    int update(ChatUser.UpdateBuilder object);

    List<ChatUser> queryChatUser(ChatUser object);

    ChatUser queryChatUserLimit1(ChatUser object);

}