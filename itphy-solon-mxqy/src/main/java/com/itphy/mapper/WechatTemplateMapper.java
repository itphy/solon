package com.itphy.mapper;

import com.itphy.pojo.WechatTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

public interface WechatTemplateMapper {

    WechatTemplate queryBeanByKey(@Param("key") String key);

}
